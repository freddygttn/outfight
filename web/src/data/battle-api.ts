import { Battle } from "../domain/models/Battle";
import { httpClient } from "./http-client";

function startNewBattle(characterId: string): Promise<Battle> {
  return httpClient
    .post(`/characters/${characterId}/battles`)
    .then((r) => r.data.data);
}

export const BattleApi = {
  startNewBattle,
};
