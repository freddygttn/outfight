import axios, { AxiosResponse, AxiosRequestConfig, AxiosError } from "axios";
import camelcaseKeys from "camelcase-keys";
import snakecaseKeys from "snakecase-keys";

export const httpClient = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  withCredentials: Boolean(process.env.REACT_APP_API_IS_CROSS_SITE),
});

// Axios middleware to convert all api responses to camelCase
httpClient.interceptors.response.use(
  (response: AxiosResponse) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    if (
      response.data &&
      (response.headers["content-type"] as string).includes("application/json")
    ) {
      response.data = camelcaseKeys(response.data, { deep: true });
    }
    return response;
  },
  (error: AxiosError) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    return Promise.reject(error);
  }
);

// Axios middleware to convert all api requests to snake_case
httpClient.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // Params in URL
    if (config.params) {
      config.params = snakecaseKeys(config.params, { deep: true });
    }
    // Data inside body
    if (config.data) {
      config.data = snakecaseKeys(config.data, { deep: true });
    }
    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  }
);
