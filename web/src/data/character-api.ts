import { Character } from "../domain/models/Character";
import { httpClient } from "./http-client";

function createCharacter(name: string): Promise<Character> {
  return httpClient
    .post("/characters", { character: { name } })
    .then((r) => r.data.data);
}

function getAllCharacters(): Promise<Character[]> {
  return httpClient.get("/characters").then((r) => r.data.data);
}

function getCharacter(characterId: string): Promise<Character> {
  return httpClient.get(`/characters/${characterId}`).then((r) => r.data.data);
}

function updateCharacter(character: Character): Promise<Character> {
  return httpClient
    .put(`/characters/${character.id}`, { character })
    .then((r) => r.data.data);
}

function deleteCharacter(characterId: string): Promise<void> {
  return httpClient.delete(`/characters/${characterId}`);
}

export const CharacterApi = {
  createCharacter,
  getAllCharacters,
  getCharacter,
  updateCharacter,
  deleteCharacter,
};
