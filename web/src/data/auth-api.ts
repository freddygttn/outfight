import { User } from "../domain/models/User";
import { httpClient } from "./http-client";

function me(): Promise<User> {
  return httpClient.get("/auth/me").then((r) => r.data.data);
}

function register(
  email: string,
  password: string,
  username: string
): Promise<User> {
  return httpClient
    .post("/auth/register", { user: { email, password, username } })
    .then((r) => r.data.data);
}

function signIn(email: string, password: string): Promise<User> {
  return httpClient
    .post("/auth/sign_in", { email, password })
    .then((r) => r.data.data);
}

function signOut(): Promise<void> {
  return httpClient.post("/auth/sign_out");
}

export const AuthApi = {
  me,
  register,
  signIn,
  signOut,
};
