import { BattleResult } from "../domain/models/BattleResult";
import { httpClient } from "./http-client";

function getCharacterResults(characterId: string): Promise<BattleResult[]> {
  return httpClient
    .get(`/characters/${characterId}/battle_results`)
    .then((r) => r.data.data);
}

export const BattleResultApi = {
  getCharacterResults,
};
