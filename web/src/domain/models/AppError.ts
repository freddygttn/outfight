export enum AppError {
  AUTH_UNKNOWN_ERROR,
  AUTH_INVALID_CREDENTIALS,
  AUTH_NOT_AVAILABLE,
  CHARACTER_UNKNOWN_ERROR,
  ARENA_UNKNOWN_ERROR,
  RESULT_UNKNOWN_ERROR,
}

export function getAppErrorMessage(error: AppError): string {
  switch (error) {
    case AppError.AUTH_INVALID_CREDENTIALS:
      return "Invalid credentials";
    case AppError.AUTH_NOT_AVAILABLE:
      return "Authentication server unavailable";
    case AppError.AUTH_UNKNOWN_ERROR:
      return "Sad. :(";
    case AppError.CHARACTER_UNKNOWN_ERROR:
      return "Something wrong happened. :(";
    case AppError.ARENA_UNKNOWN_ERROR:
      return "Something wrong happened. :(";
    case AppError.RESULT_UNKNOWN_ERROR:
      return "Something wrong happened. :(";
  }
}
