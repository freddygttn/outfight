export type BattleResult = {
  id: string;
  resolvedAt: Date;
  outcome: "VICTORY" | "DEFEAT" | "DRAW";
};
