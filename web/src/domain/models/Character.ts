export type Character = {
  id: string;
  name: string;
  rank: number;
  skillPoints: number;
  healthPoints: number;
  attack: number;
  defense: number;
  magik: number;
};

export function areCharactersEqual(c1: Character, c2: Character): boolean {
  return (
    c1.id === c2.id &&
    c1.name === c2.name &&
    c1.rank === c2.rank &&
    c1.skillPoints === c2.skillPoints &&
    c1.healthPoints === c2.healthPoints &&
    c1.attack === c2.attack &&
    c1.defense === c2.defense &&
    c1.magik === c2.magik
  );
}

export function skillCost(currentStat: number): number {
  return currentStat <= 0 ? 1 : Math.ceil(currentStat / 5);
}

export function canIncrementSkill(
  currentSkillValue: number,
  currentSkillPoints: number
): boolean {
  return currentSkillPoints >= skillCost(currentSkillValue);
}

export function canDecrementSkill(
  currentSkillValue: number,
  initialSkillValue: number
): boolean {
  return currentSkillValue > initialSkillValue;
}
