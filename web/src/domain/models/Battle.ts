import { BattleResult } from "./BattleResult";
import { Character } from "./Character";

export type Battle = {
  events: BattleEvent[];
  result: BattleResult;
  updatedCharacter: Character;
};

export type AttackResult =
  | { type: "CRITICAL"; damage: number }
  | { type: "SUCCESS"; damage: number }
  | { type: "FAILURE" };

export type BattleEvent =
  | { type: "START"; challenger: Character; opponent: Character }
  | { type: "NEW_TURN"; count: number }
  | { type: "CHALLENGER_ROLL"; value: number }
  | { type: "CHALLENGER_ATTACK"; result: AttackResult }
  | { type: "OPPONENT_ROLL"; value: number }
  | { type: "OPPONENT_ATTACK"; result: AttackResult }
  | { type: "END"; outcome: "VICTORY" | "DRAW" | "DEFEAT" };
