import { useEffect, useState } from "react";
import { arenaQuery } from "./arenae/arena-query";
import { ArenaService } from "./arenae/arena-service";
import { ArenaState } from "./arenae/arena-store";
import { AppError } from "./models/AppError";

export type ArenaHook = [
  {
    arenaState: ArenaState;
    isLoading: boolean;
    currentError: AppError | null;
  },
  typeof ArenaService
];

export function useArenaHook(): ArenaHook {
  const [arenaState, setArenaState] = useState<ArenaState>({
    type: "READY",
  });
  const [currentError, setCurrentError] = useState<AppError | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  /**
   * Manage subscription with auto-cleanup
   */
  useEffect(() => {
    const subscriptions = [
      arenaQuery.state$.subscribe({
        next: setArenaState,
      }),
      arenaQuery.selectError().subscribe({
        next: setCurrentError,
      }),
      arenaQuery.selectLoading().subscribe({
        next: setIsLoading,
      }),
    ];

    return () => {
      subscriptions.forEach((s) => s.unsubscribe());
    };
  }, []);

  return [
    {
      arenaState,
      isLoading,
      currentError,
    },
    ArenaService,
  ];
}
