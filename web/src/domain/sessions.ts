import { useEffect, useState } from "react";
import { AppError } from "./models/AppError";
import { sessionQuery } from "./sessions/session-query";
import { SessionService } from "./sessions/session-service";
import { SessionState } from "./sessions/session-store";

export type SessionHook = [
  {
    sessionState: SessionState;
    isLoading: boolean;
    currentError: AppError | null;
  },
  typeof SessionService
];

export function useSessionHook(): SessionHook {
  const [sessionState, setSessionState] = useState<SessionState>({
    type: "UNINITIALIZED",
  });
  const [currentError, setCurrentError] = useState<AppError | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  /**
   * Manage subscription with auto-cleanup
   */
  useEffect(() => {
    const subscriptions = [
      sessionQuery.state$.subscribe({
        next: setSessionState,
      }),
      sessionQuery.selectError().subscribe({
        next: setCurrentError,
      }),
      sessionQuery.selectLoading().subscribe({
        next: setIsLoading,
      }),
    ];

    return () => {
      subscriptions.forEach((s) => s.unsubscribe());
    };
  }, []);

  return [
    {
      sessionState,
      isLoading,
      currentError,
    },
    SessionService,
  ];
}
