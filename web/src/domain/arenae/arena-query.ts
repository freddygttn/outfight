import { Query } from "@datorama/akita";
import { ArenaState, arenaStore, ArenaStore } from "./arena-store";

export class ArenaQuery extends Query<ArenaState> {
  state$ = this.select();

  constructor(protected store: ArenaStore) {
    super(store);
  }
}

export const arenaQuery = new ArenaQuery(arenaStore);
