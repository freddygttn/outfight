import { Store } from "@datorama/akita";
import { Battle } from "../models/Battle";

export type ArenaState =
  | { type: "READY" }
  | { type: "FIGHTING"; battle: Battle };

export function createInitialState(): ArenaState {
  return { type: "READY" };
}

export class ArenaStore extends Store<ArenaState> {}

export const arenaStore = new ArenaStore(createInitialState(), {
  name: "arena",
});
