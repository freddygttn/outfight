import { applyTransaction } from "@datorama/akita";
import { arenaStore } from "./arena-store";
import { AppError } from "../models/AppError";
import { Character } from "../models/Character";
import { BattleApi } from "../../data/battle-api";
import { characterStore } from "../characters/character-store";
import produce from "immer";

// ----------------------------------------------------------------------------
// API
// ----------------------------------------------------------------------------
function handleArenaError() {
  applyTransaction(() => {
    arenaStore.setLoading(false);
    arenaStore.setError(AppError.ARENA_UNKNOWN_ERROR);
  });
}

function startBattle(character: Character) {
  arenaStore.setLoading(true);
  BattleApi.startNewBattle(character.id)
    .then((battle) => {
      applyTransaction(() => {
        arenaStore.setLoading(false);
        arenaStore.update({
          type: "FIGHTING",
          battle: battle,
        });
        characterStore.update(
          battle.updatedCharacter.id,
          produce((draft) => {
            draft.rank = battle.updatedCharacter.rank;
            draft.skillPoints = battle.updatedCharacter.skillPoints;
          })
        );
      });
    })
    .catch(handleArenaError);
}

// ----------------------------------------------------------------------------
// UI
// ----------------------------------------------------------------------------
function setReady() {
  arenaStore.update({ type: "READY" });
}

export const ArenaService = {
  startBattle,
  setReady,
};
