import { applyTransaction } from "@datorama/akita";
import { produce } from "immer";
import { CharacterApi } from "../../data/character-api";
import { AppError } from "../models/AppError";
import {
  canIncrementSkill,
  canDecrementSkill,
  skillCost,
  Character,
} from "../models/Character";
import { characterStore } from "./character-store";

// ----------------------------------------------------------------------------
// API
// ----------------------------------------------------------------------------
function handleCharacterError() {
  applyTransaction(() => {
    characterStore.setLoading(false);
    characterStore.setError(AppError.CHARACTER_UNKNOWN_ERROR);
  });
}

function loadCharacters() {
  characterStore.setLoading(true);
  CharacterApi.getAllCharacters()
    .then((users) => {
      applyTransaction(() => {
        characterStore.setLoading(false);
        characterStore.set(users);
        if (users.length > 0 && !Boolean(characterStore.getValue().active)) {
          characterStore.setActive(users[0].id);
        }
      });
    })
    .catch(handleCharacterError);
}

function createCharacter(name: string) {
  characterStore.setLoading(true);
  CharacterApi.createCharacter(name)
    .then((user) => {
      applyTransaction(() => {
        characterStore.setLoading(false);
        characterStore.add(user);
        characterStore.setActive(user.id);
      });
    })
    .catch(handleCharacterError);
}

function updateCharacterStats(character: Character) {
  characterStore.setLoading(true);
  CharacterApi.updateCharacter(character)
    .then((updatedCharacter) => {
      applyTransaction(() => {
        characterStore.setLoading(false);
        characterStore.update(
          updatedCharacter.id,
          produce((_draft) => {
            _draft.skillPoints = updatedCharacter.skillPoints;
            _draft.healthPoints = updatedCharacter.healthPoints;
            _draft.attack = updatedCharacter.attack;
            _draft.defense = updatedCharacter.defense;
            _draft.magik = updatedCharacter.magik;
          })
        );
        characterStore.ui.update(
          updatedCharacter.id,
          produce((draft) => {
            draft.characterEdition = updatedCharacter;
            draft.isEditing = false;
          })
        );
      });
    })
    .catch(handleCharacterError);
}

function deleteCharacter(characterId: string) {
  characterStore.setLoading(true);
  CharacterApi.deleteCharacter(characterId)
    .then(() => {
      applyTransaction(() => {
        characterStore.setLoading(false);
        characterStore.remove(characterId);
        characterStore.setActive(null);
      });
    })
    .catch(handleCharacterError);
}

// ----------------------------------------------------------------------------
// UI
// ----------------------------------------------------------------------------
function selectCharacter(id: string | null) {
  characterStore.setActive(id);
}

function selectNextCharacter() {
  characterStore.setActive({ next: true });
}

function selectPreviousCharacter() {
  characterStore.setActive({ prev: true });
}

function toggleCharacterEdition(character: Character) {
  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.isEditing = !draft.isEditing;
      draft.characterEdition = character;
    })
  );
}

function updateEditingCharacter(character: Character) {
  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition = character;
    })
  );
}

function incrementCharacterHealthPoints(character: Character) {
  if (character.skillPoints <= 0) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints -= 1;
      draft.characterEdition.healthPoints += 1;
    })
  );
}

function decrementCharacterHealthPoints(
  character: Character,
  initialCharacter: Character
) {
  if (character.healthPoints <= initialCharacter.healthPoints) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints += 1;
      draft.characterEdition.healthPoints -= 1;
    })
  );
}

function incrementCharacterAttack(character: Character) {
  if (!canIncrementSkill(character.attack, character.skillPoints)) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints -= skillCost(
        draft.characterEdition.attack
      );
      draft.characterEdition.attack += 1;
    })
  );
}

function decrementCharacterAttack(
  character: Character,
  initialCharacter: Character
) {
  if (!canDecrementSkill(character.attack, initialCharacter.attack)) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints += skillCost(character.attack - 1);
      draft.characterEdition.attack -= 1;
    })
  );
}

function incrementCharacterDefense(character: Character) {
  if (!canIncrementSkill(character.defense, character.skillPoints)) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints -= skillCost(
        draft.characterEdition.defense
      );
      draft.characterEdition.defense += 1;
    })
  );
}

function decrementCharacterDefense(
  character: Character,
  initialCharacter: Character
) {
  if (!canDecrementSkill(character.defense, initialCharacter.defense)) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints += skillCost(character.defense - 1);
      draft.characterEdition.defense -= 1;
    })
  );
}

function incrementCharacterMagik(character: Character) {
  if (!canIncrementSkill(character.magik, character.skillPoints)) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints -= skillCost(
        draft.characterEdition.magik
      );
      draft.characterEdition.magik += 1;
    })
  );
}

function decrementCharacterMagik(
  character: Character,
  initialCharacter: Character
) {
  if (!canDecrementSkill(character.magik, initialCharacter.magik)) {
    return;
  }

  characterStore.ui.update(
    character.id,
    produce((draft) => {
      draft.characterEdition.skillPoints += skillCost(character.magik - 1);
      draft.characterEdition.magik -= 1;
    })
  );
}

// ----------------------------------------------------------------------------
// Export
// ----------------------------------------------------------------------------
export const CharacterService = {
  loadCharacters,
  createCharacter,
  updateCharacterStats,
  deleteCharacter,
  selectCharacter,
  selectNextCharacter,
  selectPreviousCharacter,
  toggleCharacterEdition,
  updateEditingCharacter,
  incrementCharacterHealthPoints,
  decrementCharacterHealthPoints,
  incrementCharacterAttack,
  decrementCharacterAttack,
  incrementCharacterDefense,
  decrementCharacterDefense,
  incrementCharacterMagik,
  decrementCharacterMagik,
};
