import { EntityUIQuery, QueryEntity } from "@datorama/akita";
import {
  CharacterState,
  characterStore,
  CharacterStore,
  CharacterUIState,
} from "./character-store";

export class CharacterQuery extends QueryEntity<CharacterState> {
  ui!: EntityUIQuery<CharacterUIState>;

  constructor(protected store: CharacterStore) {
    super(store);
    this.createUIQuery();
  }
}

export const characterQuery = new CharacterQuery(characterStore);
