import {
  ActiveState,
  EntityStore,
  EntityState,
  EntityUIStore,
  StoreConfigOptions,
} from "@datorama/akita";
import { Character } from "../models/Character";

export type CharacterUI = {
  isEditing: boolean;
  characterEdition: Character;
};

export interface CharacterState
  extends EntityState<Character, string>,
    ActiveState {}

export interface CharacterUIState
  extends EntityState<CharacterUI>,
    ActiveState {}

export class CharacterStore extends EntityStore<CharacterState> {
  ui!: EntityUIStore<CharacterUIState>;

  constructor(
    initialState: Partial<CharacterState>,
    config: Partial<StoreConfigOptions>
  ) {
    super(initialState, config);
    const defaults = (entity: Character) => ({
      isEditing: false,
      characterEdition: entity,
    });
    this.createUIStore().setInitialEntityState(defaults);
  }
}

export const characterStore = new CharacterStore(
  {},
  {
    name: "character",
  }
);
