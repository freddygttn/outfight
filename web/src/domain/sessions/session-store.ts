import { Store } from "@datorama/akita";
import { User } from "../models/User";

export type SessionState =
  | { type: "UNINITIALIZED" }
  | { type: "SIGNED_IN"; user: User }
  | { type: "SIGNED_OUT" };

export function createInitialState(): SessionState {
  return { type: "UNINITIALIZED" };
}

export class SessionStore extends Store<SessionState> {}

export const sessionStore = new SessionStore(createInitialState(), {
  name: "session",
});
