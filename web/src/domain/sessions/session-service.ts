import { applyTransaction } from "@datorama/akita";
import { AuthApi } from "../../data/auth-api";
import { sessionStore } from "./session-store";
import { User } from "../models/User";
import { AppError } from "../models/AppError";

function signedIn(user: User) {
  applyTransaction(() => {
    sessionStore.setLoading(false);
    sessionStore.update({ type: "SIGNED_IN", user });
  });
}

function signedOut() {
  applyTransaction(() => {
    sessionStore.setLoading(false);
    sessionStore.update({ type: "SIGNED_OUT" });
  });
}

function handleAuthApiError(status: number) {
  let error = AppError.AUTH_UNKNOWN_ERROR;
  switch (true) {
    case status >= 400 && status < 500:
      error = AppError.AUTH_INVALID_CREDENTIALS;
      break;
    case status >= 500 && status < 600:
      error = AppError.AUTH_NOT_AVAILABLE;
      break;
  }
  applyTransaction(() => {
    sessionStore.setLoading(false);
    sessionStore.setError(error);
  });
}

function fetchCurrentUser() {
  sessionStore.setLoading(true);
  AuthApi.me().then(signedIn).catch(signedOut);
}

function signIn(email: string, password: string) {
  sessionStore.setLoading(true);
  AuthApi.signIn(email, password)
    .then(signedIn)
    .catch((e) => handleAuthApiError(e.status));
}

function signOut() {
  sessionStore.setLoading(true);
  AuthApi.signOut()
    .then(signedOut)
    .catch((e) => handleAuthApiError(e.status));
}

function register(email: string, password: string, username: string) {
  sessionStore.setLoading(true);
  AuthApi.register(email, password, username)
    .then(signedIn)
    .catch((e) => handleAuthApiError(e.status));
}

export const SessionService = {
  fetchCurrentUser,
  signIn,
  signOut,
  register,
};
