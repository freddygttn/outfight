import { Query } from "@datorama/akita";
import { SessionState, sessionStore, SessionStore } from "./session-store";

export class SessionQuery extends Query<SessionState> {
  state$ = this.select();

  constructor(protected store: SessionStore) {
    super(store);
  }
}

export const sessionQuery = new SessionQuery(sessionStore);
