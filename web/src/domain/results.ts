import { useEffect, useState } from "react";
import { AppError } from "./models/AppError";
import { BattleResult } from "./models/BattleResult";
import { resultQuery } from "./results/result-query";
import { ResultService } from "./results/result-service";

export type ResultHook = [
  {
    results: BattleResult[];
    isLoading: boolean;
    currentError: AppError | null;
  },
  typeof ResultService
];

export function useResultHook(): ResultHook {
  const [results, setResults] = useState<BattleResult[]>([]);
  const [currentError, setCurrentError] = useState<AppError | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  /**
   * Manage subscription with auto-cleanup
   */
  useEffect(() => {
    const subscriptions = [
      resultQuery.selectAll().subscribe({
        next: setResults,
      }),
      resultQuery.selectError().subscribe({
        next: setCurrentError,
      }),
      resultQuery.selectLoading().subscribe({
        next: setIsLoading,
      }),
    ];

    return () => {
      subscriptions.forEach((s) => s.unsubscribe());
    };
  }, []);

  return [
    {
      results,
      isLoading,
      currentError,
    },
    ResultService,
  ];
}
