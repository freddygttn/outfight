import { QueryEntity } from "@datorama/akita";
import { ResultState, resultStore, ResultStore } from "./result-store";

export class ResultQuery extends QueryEntity<ResultState> {
  constructor(protected store: ResultStore) {
    super(store);
  }
}

export const resultQuery = new ResultQuery(resultStore);
