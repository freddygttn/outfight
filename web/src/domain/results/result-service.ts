import { applyTransaction } from "@datorama/akita";
import { BattleResultApi } from "../../data/battle-result-api";
import { AppError } from "../models/AppError";
import {} from "../models/BattleResult";
import { resultStore } from "./result-store";

// ----------------------------------------------------------------------------
// API
// ----------------------------------------------------------------------------
function handleResultError() {
  applyTransaction(() => {
    resultStore.setLoading(false);
    resultStore.setError(AppError.RESULT_UNKNOWN_ERROR);
  });
}

function loadCharacterResults(characterId: string) {
  resultStore.setLoading(true);
  BattleResultApi.getCharacterResults(characterId)
    .then((results) => {
      applyTransaction(() => {
        resultStore.setLoading(false);
        resultStore.set(results);
      });
    })
    .catch(handleResultError);
}
// ----------------------------------------------------------------------------
// Export
// ----------------------------------------------------------------------------
export const ResultService = {
  loadCharacterResults,
};
