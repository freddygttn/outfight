import { ActiveState, EntityStore, EntityState } from "@datorama/akita";
import { BattleResult } from "../models/BattleResult";

export interface ResultState
  extends EntityState<BattleResult, string>,
    ActiveState {}

export class ResultStore extends EntityStore<ResultState> {}

export const resultStore = new ResultStore(
  {},
  {
    name: "result",
  }
);
