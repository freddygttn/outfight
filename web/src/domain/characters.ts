import { useEffect, useState } from "react";
import { filter, mergeMap } from "rxjs/operators";
import { characterQuery } from "./characters/character-query";
import { CharacterService } from "./characters/character-service";
import { CharacterUI } from "./characters/character-store";
import { AppError } from "./models/AppError";
import { Character } from "./models/Character";

export type CharacterHook = [
  {
    characters: Character[];
    activeCharacter?: Character;
    activeCharacterUI?: CharacterUI;
    isLoading: boolean;
    currentError: AppError | null;
  },
  typeof CharacterService
];

export function useCharacterHook(): CharacterHook {
  const [characters, setCharacters] = useState<Character[]>([]);
  const [activeCharacter, setActiveCharacter] =
    useState<Character | undefined>(undefined);
  const [activeCharacterUI, setActiveCharacterUI] =
    useState<CharacterUI | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [currentError, setCurrentError] = useState<AppError | null>(null);

  /**
   * Manage subscription with auto-cleanup
   */
  useEffect(() => {
    const subscriptions = [
      characterQuery.selectAll().subscribe({
        next: setCharacters,
      }),
      characterQuery.selectActive().subscribe({
        next: setActiveCharacter,
      }),
      characterQuery.selectError().subscribe({
        next: setCurrentError,
      }),
      characterQuery.selectLoading().subscribe({
        next: setIsLoading,
      }),
      characterQuery
        .selectActiveId()
        .pipe(
          filter((id) => id !== undefined),
          mergeMap((id) => characterQuery.ui.selectEntity(id))
        )
        .subscribe({
          next: setActiveCharacterUI,
        }),
    ];

    return () => {
      subscriptions.forEach((s) => s.unsubscribe());
    };
  }, []);

  // --------------------------------------------------------------------------
  return [
    { characters, activeCharacter, activeCharacterUI, isLoading, currentError },
    CharacterService,
  ];
}
