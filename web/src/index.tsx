import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./ui/App";
import reportWebVitals from "./reportWebVitals";
import { enableAkitaProdMode } from "@datorama/akita";
import { enableAllPlugins } from "immer";

// Enable all plugins by default for Immer
// * This can be easily removed if we don't need it after all
enableAllPlugins();

// Akita prod mode
if (process.env.NODE_ENV === "production") {
  enableAkitaProdMode();
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
