import React from "react";
import ReactDOM from "react-dom";
import BaseButton from "./BaseButton";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BaseButton>
      {(classes) => <button className={classes}></button>}
    </BaseButton>,
    div
  );
});
