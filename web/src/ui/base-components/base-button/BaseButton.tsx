import classNames from "classnames";
import React from "react";

export type BaseButtonProps = {
  className?: string;
  disabled?: boolean;
  variant?: "default" | "primary" | "danger" | "outlined";
  size?: "custom" | "sm" | "md" | "lg" | "xl";
};

type BaseButtonInternalProps = BaseButtonProps & {
  children: (classes: string) => React.ReactNode;
};

const BaseButton = ({
  children,
  className,
  disabled = false,
  variant = "default",
  size = "md",
}: BaseButtonInternalProps) => {
  const classes = classNames(
    "border focus:outline-none ring-opacity-80",
    className,
    {
      "py-0.5 px-4 rounded": size === "sm",
      "py-1 px-8 rounded-md": size === "md",
      "py-2 px-12 text-lg rounded-lg": size === "lg",
      "py-4 px-16 text-xl rounded-xl": size === "xl",
      "bg-gray-700 ring-gray-800 border-gray-700": variant === "default",
      "bg-yellow-400 border-yellow-400 ring-yellow-500 text-gray-900":
        variant === "primary",
      "bg-red-600 border-red-600 ring-red-700": variant === "danger",
      "bg-transparent ring-gray-300": variant === "outlined",
      "hover:ring focus:ring": !disabled,
      "opacity-50 cursor-not-allowed hover:ring-0 focus:ring-0": disabled,
    }
  );
  return <>{children(classes)}</>;
};

export default BaseButton;
