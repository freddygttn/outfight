import { Link } from "@reach/router";
import React from "react";
import BaseButton, { BaseButtonProps } from "../base-button/BaseButton";

type LinkButtonProps = BaseButtonProps & {
  to: string;
  children?: React.ReactNode;
};

const LinkButton = ({ to, children, ...props }: LinkButtonProps) => {
  return (
    <BaseButton {...props}>
      {(classes) => (
        <Link to={props.disabled ? "" : to} className={classes}>
          {children}
        </Link>
      )}
    </BaseButton>
  );
};

export default LinkButton;
