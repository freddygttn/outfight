import React from "react";
import BaseButton, { BaseButtonProps } from "../base-button/BaseButton";

type ButtonProps = BaseButtonProps & {
  children?: React.ReactNode;
  type?: "button" | "submit" | "reset";
  title?: string;
  onClick?: (e: React.MouseEvent) => void;
};

const Button = ({
  children,
  type = "button",
  title,
  onClick = () => {},
  ...props
}: ButtonProps) => {
  return (
    <BaseButton {...props}>
      {(classes) => (
        <button
          className={classes}
          type={type}
          onClick={onClick}
          disabled={props.disabled}
          title={title}
        >
          {children}
        </button>
      )}
    </BaseButton>
  );
};

export default Button;
