import { Link, Redirect, Router } from "@reach/router";
import React, { useEffect } from "react";
import { useSessionHook } from "../domain/sessions";
import Arena from "./pages/arena/Arena";
import Dashboard from "./pages/dashboard/Dashboard";
import Register from "./pages/register/Register";
import { routes } from "./pages/routes";
import SignIn from "./pages/sign-in/SignIn";

const App = () => {
  const [state, service] = useSessionHook();
  const isSignedIn = state.sessionState.type === "SIGNED_IN";
  const isInitializing = state.sessionState.type === "UNINITIALIZED";
  useEffect(() => {
    service.fetchCurrentUser();
  }, [service]);

  return (
    <div className="min-h-screen bg-gray-800 text-gray-100">
      <header className="flex items-center justify-center h-12 relative bg-gray-900">
        {isSignedIn ? (
          <div className="h-12 absolute top-0 left-0 px-4 flex items-center text-gray-300">
            {state.sessionState.type === "SIGNED_IN" &&
              state.sessionState.user.username}
          </div>
        ) : null}
        <h1 className="uppercase tracking-widest">
          <Link to={routes.dashboard()}>Outfight</Link>
        </h1>
        {isSignedIn ? (
          <button
            className="h-12 absolute top-0 right-0 px-4 text-gray-300"
            onClick={() => service.signOut()}
          >
            Sign Out
          </button>
        ) : null}
      </header>
      <main>
        {isInitializing ? (
          <div className="flex justify-center items-center">Loading...</div>
        ) : (
          <>
            <Router>
              {isSignedIn ? (
                <>
                  <Dashboard path={routes.dashboard()} />
                  <Arena path={routes.arena()} />
                  <Redirect from="/" to={routes.dashboard()} noThrow default />
                </>
              ) : (
                <>
                  <SignIn path={routes.signIn()} />
                  <Register path={routes.register()} />
                  <Redirect from="/" to={routes.signIn()} noThrow default />
                </>
              )}
            </Router>
          </>
        )}
      </main>
    </div>
  );
};

export default App;
