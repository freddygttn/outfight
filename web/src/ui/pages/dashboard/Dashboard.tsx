import { RouteComponentProps } from "@reach/router";
import React, { useEffect, useState } from "react";
import { useCharacterHook } from "../../../domain/characters";
import Button from "../../base-components/button/Button";
import LinkButton from "../../base-components/link-button/LinkButton";
import { routes } from "../routes";
import CharacterCreateForm from "./components/character-create-form/CharacterCreateForm";
import CharacterSelector from "./components/character-selector/CharacterSelector";
import CharacterSheet from "./components/character-sheet/CharacterSheet";
import DeletionModal from "./components/deletion-modal/DeletionModal";

const Dashboard = (_props: RouteComponentProps) => {
  const [state, service] = useCharacterHook();
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const character = state.activeCharacter;

  useEffect(() => {
    service.loadCharacters();
  }, [service]);

  return (
    <div className="">
      {showConfirmModal && (
        <DeletionModal
          onCancel={() => setShowConfirmModal(false)}
          onConfirm={() => {
            if (character) {
              service.deleteCharacter(character.id);
            }
            setShowConfirmModal(false);
          }}
        />
      )}
      <CharacterSelector
        className="mt-8"
        characters={state.characters}
        selectedId={state.activeCharacter?.id}
        onSelect={service.selectCharacter}
      />
      {character ? (
        <>
          <div className="flex justify-between mx-8 mt-8">
            <Button
              size="sm"
              className="mr-1"
              onClick={service.selectPreviousCharacter}
            >
              &lt; Prev
            </Button>
            <Button
              size="sm"
              className="mx-1"
              onClick={() => setShowConfirmModal(true)}
            >
              Delete
            </Button>
            <Button
              size="sm"
              className="ml-1"
              onClick={service.selectNextCharacter}
            >
              Next &gt;
            </Button>
          </div>
          <div className="mt-2">
            <CharacterSheet className="mx-8" />
          </div>
          <div className="flex justify-center mt-16">
            <LinkButton size="xl" to={routes.arena()} variant="primary">
              Enter the Arena
            </LinkButton>
          </div>
        </>
      ) : (
        <CharacterCreateForm
          className="mx-8 mt-8"
          onSubmit={service.createCharacter}
        />
      )}
    </div>
  );
};

export default Dashboard;
