import classNames from "classnames";
import React from "react";
import { Character } from "../../../../../domain/models/Character";

type CharacterSelectorProps = {
  className?: string;
  characters: Character[];
  selectedId?: string;
  onSelect?: (id: string | null) => void;
};

const CharacterSelector = ({
  className,
  characters,
  selectedId,
  onSelect = () => {},
}: CharacterSelectorProps) => {
  return (
    <div className={classNames("flex items-center justify-center", className)}>
      {characters.length < 10 && (
        <div
          onClick={() => onSelect(null)}
          className={classNames(
            "mx-1 h-8 w-8 rounded-full border border-gray-100 text-center",
            {
              "bg-yellow-900": !Boolean(selectedId),
              "bg-gray-700": Boolean(selectedId),
            }
          )}
        >
          <span className="relative top-0.5">+</span>
        </div>
      )}
      {characters.map((character) => (
        <div
          key={`selector-${character.id}`}
          onClick={() => onSelect(character.id)}
          className={classNames("mx-1 h-8 w-8 rounded-full text-center", {
            "bg-yellow-900": character.id === selectedId,
            "bg-gray-700": character.id !== selectedId,
          })}
        >
          <span className="relative top-0.5">{character.name.charAt(0)}</span>
        </div>
      ))}
    </div>
  );
};

export default CharacterSelector;
