import React from "react";
import ReactDOM from "react-dom";
import CharacterSelector from "./CharacterSelector";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<CharacterSelector characters={[]} />, div);
});
