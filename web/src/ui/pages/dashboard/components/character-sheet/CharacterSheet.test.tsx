import React from "react";
import ReactDOM from "react-dom";
import { Character } from "../../../../../domain/models/Character";
import CharacterSheet from "./CharacterSheet";

it("renders without crashing", () => {
  const character: Character = {
    id: "abc",
    name: "some name",
    rank: 1,
    healthPoints: 10,
    skillPoints: 10,
    attack: 1,
    defense: 1,
    magik: 1,
  };
  const div = document.createElement("div");
  ReactDOM.render(<CharacterSheet character={character} />, div);
});
