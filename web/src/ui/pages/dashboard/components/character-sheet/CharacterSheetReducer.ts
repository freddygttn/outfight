import produce from "immer";
import {
  canDecrementSkill,
  canIncrementSkill,
  Character,
  skillCost,
} from "../../../../../domain/models/Character";

export type CharacterSheetState = {
  isEditing: boolean;
  initialCharacter: Character;
  character: Character;
};

export type CharacterSheetAction =
  | { type: "TOGGLE_EDITING" }
  | { type: "RESET" }
  | { type: "INCREMENT_HEALTH_POINT" }
  | { type: "DECREMENT_HEALTH_POINT" }
  | { type: "INCREMENT_ATTACK" }
  | { type: "DECREMENT_ATTACK" }
  | { type: "INCREMENT_DEFENSE" }
  | { type: "DECREMENT_DEFENSE" }
  | { type: "INCREMENT_MAGIK" }
  | { type: "DECREMENT_MAGIK" };

export function createCharacterSheetReducer(initialState: CharacterSheetState) {
  return produce((draft: CharacterSheetState, action: CharacterSheetAction) => {
    switch (action.type) {
      case "TOGGLE_EDITING":
        draft.isEditing = !draft.isEditing;
        break;
      case "RESET":
        draft.character = draft.initialCharacter;
        break;
      case "INCREMENT_HEALTH_POINT":
        if (draft.character.skillPoints >= 1) {
          draft.character.skillPoints -= 1;
          draft.character.healthPoints += 1;
        }
        break;
      case "DECREMENT_HEALTH_POINT":
        if (
          draft.character.healthPoints > draft.initialCharacter.healthPoints
        ) {
          draft.character.skillPoints += 1;
          draft.character.healthPoints -= 1;
        }
        break;
      case "INCREMENT_ATTACK":
        if (
          canIncrementSkill(draft.character.attack, draft.character.skillPoints)
        ) {
          draft.character.skillPoints -= skillCost(draft.character.attack);
          draft.character.attack += 1;
        }
        break;
      case "DECREMENT_ATTACK":
        if (
          canDecrementSkill(
            draft.character.attack,
            draft.initialCharacter.attack
          )
        ) {
          draft.character.skillPoints += skillCost(draft.character.attack - 1);
          draft.character.attack -= 1;
        }
        break;
      case "INCREMENT_DEFENSE":
        if (
          canIncrementSkill(
            draft.character.defense,
            draft.character.skillPoints
          )
        ) {
          draft.character.skillPoints -= skillCost(draft.character.defense);
          draft.character.defense += 1;
        }
        break;
      case "DECREMENT_DEFENSE":
        if (
          canDecrementSkill(
            draft.character.defense,
            draft.initialCharacter.defense
          )
        ) {
          draft.character.skillPoints += skillCost(draft.character.defense - 1);
          draft.character.defense -= 1;
        }
        break;
      case "INCREMENT_MAGIK":
        if (
          canIncrementSkill(draft.character.magik, draft.character.skillPoints)
        ) {
          draft.character.skillPoints -= skillCost(draft.character.magik);
          draft.character.magik += 1;
        }
        break;
      case "DECREMENT_MAGIK":
        if (
          canDecrementSkill(draft.character.magik, draft.initialCharacter.magik)
        ) {
          draft.character.skillPoints += skillCost(draft.character.magik - 1);
          draft.character.magik -= 1;
        }
        break;
    }
  }, initialState);
}
