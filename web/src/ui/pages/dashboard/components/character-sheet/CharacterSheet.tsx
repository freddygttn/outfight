import classNames from "classnames";
import React from "react";
import { useCharacterHook } from "../../../../../domain/characters";
import {
  areCharactersEqual,
  canDecrementSkill,
  canIncrementSkill,
  skillCost,
} from "../../../../../domain/models/Character";
import Button from "../../../../base-components/button/Button";
import CharacterSheetSkill from "../character-sheet-skill/CharacterSheetSkill";

export type CharacterSheetProps = {
  className?: string;
};

const CharacterSheet = ({ className }: CharacterSheetProps) => {
  const [state, service] = useCharacterHook();

  const character = state.activeCharacter;
  const characterEdition = state.activeCharacterUI?.characterEdition;

  if (character === undefined || characterEdition === undefined) {
    return null;
  }

  const isEditing = character.skillPoints > 0;

  return (
    <div className={classNames("rounded-lg p-4 bg-yellow-900", className)}>
      <div className="flex justify-between items-baseline">
        <div className="flex justify-start items-baseline border-b-2 border-yellow-700">
          <h3 className="text-2xl">{character.name}</h3>
          <h4 className="mx-1 text-gray-300">Rank {character.rank}</h4>
        </div>
        {isEditing ? (
          <div>
            <span className="inline-block">
              {characterEdition.skillPoints} available point
              {characterEdition.skillPoints > 1 ? "s" : ""}
            </span>
          </div>
        ) : null}
      </div>
      <div className="mt-4">
        <div className="grid grid-cols-2 md:grid-cols-4">
          <CharacterSheetSkill
            name="Health Points"
            isEditing={isEditing}
            currentValue={characterEdition.healthPoints}
            previousCost={1}
            nextCost={1}
            onDecrement={() =>
              service.decrementCharacterHealthPoints(
                characterEdition,
                character
              )
            }
            onIncrement={() =>
              service.incrementCharacterHealthPoints(characterEdition)
            }
            canDecrement={
              characterEdition.healthPoints > character.healthPoints
            }
            canIncrement={characterEdition.skillPoints > 0}
          />

          <CharacterSheetSkill
            name="Attack"
            isEditing={isEditing}
            currentValue={characterEdition.attack}
            previousCost={skillCost(characterEdition.attack - 1)}
            nextCost={skillCost(characterEdition.attack)}
            onDecrement={() =>
              service.decrementCharacterAttack(characterEdition, character)
            }
            onIncrement={() =>
              service.incrementCharacterAttack(characterEdition)
            }
            canDecrement={canDecrementSkill(
              characterEdition.attack,
              character.attack
            )}
            canIncrement={canIncrementSkill(
              characterEdition.attack,
              characterEdition.skillPoints
            )}
          />
          <CharacterSheetSkill
            name="Defense"
            isEditing={isEditing}
            currentValue={characterEdition.defense}
            previousCost={skillCost(characterEdition.defense - 1)}
            nextCost={skillCost(characterEdition.defense)}
            onDecrement={() =>
              service.decrementCharacterDefense(characterEdition, character)
            }
            onIncrement={() =>
              service.incrementCharacterDefense(characterEdition)
            }
            canDecrement={canDecrementSkill(
              characterEdition.defense,
              character.defense
            )}
            canIncrement={canIncrementSkill(
              characterEdition.defense,
              characterEdition.skillPoints
            )}
          />
          <CharacterSheetSkill
            name="Magik"
            isEditing={isEditing}
            currentValue={characterEdition.magik}
            previousCost={skillCost(characterEdition.magik - 1)}
            nextCost={skillCost(characterEdition.magik)}
            onDecrement={() =>
              service.decrementCharacterMagik(characterEdition, character)
            }
            onIncrement={() =>
              service.incrementCharacterMagik(characterEdition)
            }
            canDecrement={canDecrementSkill(
              characterEdition.magik,
              character.magik
            )}
            canIncrement={canIncrementSkill(
              characterEdition.magik,
              characterEdition.skillPoints
            )}
          />
        </div>
        {isEditing ? (
          <div className="mt-4 text-center">
            <Button
              className="mr-2"
              variant="outlined"
              onClick={() => service.updateEditingCharacter(character)}
              disabled={
                areCharactersEqual(characterEdition, character) ||
                state.isLoading
              }
            >
              Reset
            </Button>
            <Button
              className=""
              onClick={() => service.updateCharacterStats(characterEdition)}
              disabled={
                areCharactersEqual(characterEdition, character) ||
                state.isLoading
              }
            >
              Validate
            </Button>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default CharacterSheet;
