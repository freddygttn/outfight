import React from "react";
import Button from "../../../../base-components/button/Button";

type CharacterSheetSkillProps = {
  name: string;
  currentValue: number;
  previousCost: number;
  nextCost: number;
  onIncrement: () => void;
  onDecrement: () => void;
  canIncrement: boolean;
  canDecrement: boolean;
  isEditing: boolean;
};

const CharacterSheetSkill = ({
  name,
  currentValue,
  previousCost,
  nextCost,
  onIncrement,
  onDecrement,
  canIncrement,
  canDecrement,
  isEditing,
}: CharacterSheetSkillProps) => {
  return (
    <div className="CharacterSheetSkill">
      <div className="flex justify-center content-center items-baseline">
        {isEditing ? (
          <Button
            className="rounded-lg p-1 mr-2"
            size="custom"
            variant="outlined"
            onClick={onDecrement}
            disabled={!canDecrement}
          >
            - ({previousCost})
          </Button>
        ) : null}
        <span className="text-4xl">{currentValue}</span>
        {isEditing ? (
          <Button
            className="rounded-lg p-1 ml-2"
            size="custom"
            variant="outlined"
            onClick={onIncrement}
            disabled={!canIncrement}
          >
            + ({nextCost})
          </Button>
        ) : null}
      </div>
      <div className="text-center text-gray-300">{name}</div>
    </div>
  );
};

export default CharacterSheetSkill;
