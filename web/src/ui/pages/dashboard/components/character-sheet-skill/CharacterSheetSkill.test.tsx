import React from "react";
import ReactDOM from "react-dom";
import CharacterSheetSkill from "./CharacterSheetSkill";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <CharacterSheetSkill
      name="Attack"
      currentValue={1}
      previousCost={1}
      nextCost={1}
      canDecrement={true}
      canIncrement={true}
      isEditing={true}
      onIncrement={() => {}}
      onDecrement={() => {}}
    />,
    div
  );
});
