import classNames from "classnames";
import React, { useState } from "react";
import Button from "../../../../base-components/button/Button";

type CharacterCreateFormProps = {
  className?: string;
  onSubmit?: (characterName: string) => void;
};

const CharacterCreateForm = ({
  className,
  onSubmit = () => {},
}: CharacterCreateFormProps) => {
  const [newCharacterName, setNewCharacterName] = useState("");

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (newCharacterName.trim() === "") {
      return;
    }
    onSubmit(newCharacterName);
  };
  return (
    <div className={classNames(className)}>
      <h3 className="text-xl">Create a new character</h3>
      <form className="p-8 bg-gray-700 mt-2" onSubmit={handleSubmit}>
        <label className="block">
          <span className="">Character name</span>
          <input
            className="mt-0 block w-full px-1 bg-transparent border-0 border-b-2 border-gray-900 focus:ring-0 focus:border-yellow-400"
            type="text"
            onChange={(e) => setNewCharacterName(e.currentTarget.value)}
          ></input>
        </label>
        <div className="text-center mt-8">
          <Button
            className=""
            type="submit"
            variant="primary"
            disabled={newCharacterName.trim() === ""}
          >
            Create
          </Button>
        </div>
      </form>
    </div>
  );
};

export default CharacterCreateForm;
