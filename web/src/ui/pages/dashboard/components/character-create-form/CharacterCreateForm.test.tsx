import React from "react";
import ReactDOM from "react-dom";
import CharacterCreateForm from "./CharacterCreateForm";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<CharacterCreateForm />, div);
});
