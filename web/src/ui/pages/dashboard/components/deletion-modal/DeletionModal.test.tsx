import React from "react";
import ReactDOM from "react-dom";
import DeletionModal from "./DeletionModal";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<DeletionModal />, div);
});
