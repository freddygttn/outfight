import React from "react";
import Button from "../../../../base-components/button/Button";

type DeletionModalProps = {
  onCancel?: () => void;
  onConfirm?: () => void;
};

const DeletionModal = ({
  onCancel = () => {},
  onConfirm = () => {},
}: DeletionModalProps) => {
  return (
    <div className="fixed z-50 top-0 left-0 bg-opacity-80 bg-gray-800 h-screen w-screen flex justify-center items-center content-center">
      <div className="bg-gray-700 rounded-lg p-8 w-screen mx-4">
        <h2 className="text-2xl">Confirm deletion</h2>
        <p className="mt-4 text-gray-400">
          This character won't exist anymore. You cannot go back.
        </p>
        <div className="mt-8 text-right">
          <Button
            size="sm"
            variant="outlined"
            className="mr-1"
            onClick={() => onCancel()}
          >
            Cancel
          </Button>
          <Button
            size="sm"
            variant="danger"
            className="ml-1"
            onClick={() => onConfirm()}
          >
            Delete
          </Button>
        </div>
      </div>
    </div>
  );
};

export default DeletionModal;
