export const routes = {
  signIn: () => "/sign_in",
  register: () => "/register",
  dashboard: () => "/",
  arena: () => `/arena`,
};
