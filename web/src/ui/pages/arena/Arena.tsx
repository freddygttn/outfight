import { RouteComponentProps } from "@reach/router";
import React, { useEffect } from "react";
import { useArenaHook } from "../../../domain/arenae";
import { useCharacterHook } from "../../../domain/characters";
import { useResultHook } from "../../../domain/results";
import Button from "../../base-components/button/Button";
import LinkButton from "../../base-components/link-button/LinkButton";
import { routes } from "../routes";
import FightView from "./components/fight-view/FightView";
import ResultView from "./components/result-view/ResultView";
import { differenceInHours } from "date-fns";

type ArenaProps = {} & RouteComponentProps<{ characterId: string }>;

const Arena = (_props: ArenaProps) => {
  const [{ arenaState }, service] = useArenaHook();
  const [{ activeCharacter }] = useCharacterHook();
  const [{ results, isLoading }, resultService] = useResultHook();
  let canFight =
    (!isLoading && results.length === 0) ||
    (results.length > 0 && results[0].outcome !== "DEFEAT");
  if (results.length > 0 && results[0].outcome === "DEFEAT") {
    canFight =
      differenceInHours(new Date(), new Date(results[0].resolvedAt)) >= 1;
  }

  useEffect(() => {
    if (activeCharacter) {
      resultService.loadCharacterResults(activeCharacter.id);
    }
  }, [resultService, activeCharacter]);

  switch (arenaState.type) {
    case "READY":
      return (
        <div className="">
          <div className="mt-8">
            {activeCharacter ? (
              <>
                <p className="px-8 text-center">
                  You have chosen{" "}
                  <span className="font-bold">
                    {activeCharacter.name} (rank {activeCharacter.rank})
                  </span>{" "}
                  to fight in the Arena
                </p>
                {isLoading ? (
                  <div className="mt-8 mx-8 text-center">
                    Loading results...
                  </div>
                ) : (
                  <ResultView className="mt-8 mx-8" results={results} />
                )}
              </>
            ) : (
              <p className="px-8 text-center">
                You must select a character first.
              </p>
            )}
            {activeCharacter && !canFight && (
              <p className="px-8 text-center mt-2">
                This character is still resting... Come back later!
              </p>
            )}
          </div>
          <div className="flex justify-center mt-8">
            <LinkButton className="mx-1" to={routes.dashboard()}>
              Choose character
            </LinkButton>
            {activeCharacter && canFight && (
              <Button
                className="mx-1"
                variant="primary"
                onClick={() => service.startBattle(activeCharacter)}
                disabled={isLoading}
              >
                Fight
              </Button>
            )}
          </div>
        </div>
      );
    case "FIGHTING":
      return (
        <FightView battle={arenaState.battle} onLeave={service.setReady} />
      );
  }
};

export default Arena;
