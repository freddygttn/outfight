import React from "react";
import ReactDOM from "react-dom";
import FightLog from "./FightLog";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<FightLog events={[]} />, div);
});
