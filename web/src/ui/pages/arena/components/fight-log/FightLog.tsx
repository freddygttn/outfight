import classNames from "classnames";
import React from "react";
import { BattleEvent } from "../../../../../domain/models/Battle";
import FightLogEntry from "../fight-log-entry/FightLogEntry";

export type FightLogProps = {
  className?: string;
  events: BattleEvent[];
};

const FightLog = ({ className, events }: FightLogProps) => {
  return (
    <div className={classNames("p-4 bg-gray-700 rounded", className)}>
      {[...events].reverse().map((event, index) => {
        switch (event.type) {
          case "START":
            return (
              <FightLogEntry key={`log-entry-${index}`}>
                The fight begins!
              </FightLogEntry>
            );
          case "NEW_TURN":
            return (
              <FightLogEntry key={`log-entry-${index}`}>
                It is time for turn{" "}
                <span className="text-blue-400">{event.count}</span>
              </FightLogEntry>
            );
          case "CHALLENGER_ROLL":
            return (
              <FightLogEntry key={`log-entry-${index}`}>
                Your challenger is preparing an attack...
              </FightLogEntry>
            );
          case "CHALLENGER_ATTACK":
            switch (event.result.type) {
              case "CRITICAL":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    Wow! It's a{" "}
                    <span className="text-red-400">critical hit</span>. The
                    opponent takes{" "}
                    <span className="text-red-400">{event.result.damage}</span>{" "}
                    damage!
                  </FightLogEntry>
                );
              case "SUCCESS":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    <span>Hit</span>! The opponent takes{" "}
                    <span className="text-red-400">{event.result.damage}</span>{" "}
                    damage.
                  </FightLogEntry>
                );
              case "FAILURE":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    The attack fails...
                  </FightLogEntry>
                );
              default:
                return null;
            }
          case "OPPONENT_ROLL":
            return (
              <FightLogEntry key={`log-entry-${index}`}>
                The opponent is going to ripost...
              </FightLogEntry>
            );
          case "OPPONENT_ATTACK":
            switch (event.result.type) {
              case "CRITICAL":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    Ouch! It's a{" "}
                    <span className="text-pink-400">critical hit</span>. Your
                    challenger takes{" "}
                    <span className="text-pink-400">{event.result.damage}</span>{" "}
                    damage!
                  </FightLogEntry>
                );
              case "SUCCESS":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    <span>Hit</span>! Your challenger takes{" "}
                    <span className="text-pink-400">{event.result.damage}</span>{" "}
                    damage.
                  </FightLogEntry>
                );
              case "FAILURE":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    The attack fails...
                  </FightLogEntry>
                );
              default:
                return null;
            }
          case "END":
            switch (event.outcome) {
              case "VICTORY":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    Let's celebrate, it's a{" "}
                    <span className="text-yellow-400">Victory</span>!
                  </FightLogEntry>
                );
              case "DRAW":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    This fight is going nowhere. It's a{" "}
                    <span className="text-purple-600">draw</span>.
                  </FightLogEntry>
                );
              case "DEFEAT":
                return (
                  <FightLogEntry key={`log-entry-${index}`}>
                    Your challenger is down... It will take time to recover from
                    this <span className="text-red-600">defeat</span>.
                  </FightLogEntry>
                );
              default:
                return null;
            }
          default:
            return null;
        }
      })}
    </div>
  );
};

export default FightLog;
