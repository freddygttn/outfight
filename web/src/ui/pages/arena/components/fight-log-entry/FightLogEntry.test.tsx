import React from "react";
import ReactDOM from "react-dom";
import FightLogEntry from "./FightLogEntry";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <FightLogEntry>
      <div>Test</div>
    </FightLogEntry>,
    div
  );
});
