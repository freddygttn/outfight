import classNames from "classnames";
import React from "react";

export type FightLogEntryProps = {
  className?: string;
  children?: React.ReactNode;
};

const FightLogEntry = ({ className, children }: FightLogEntryProps) => {
  return <div className={classNames("pb-0.5", className)}>{children}</div>;
};

export default FightLogEntry;
