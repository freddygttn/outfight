import { BattleEvent } from "../../../../../domain/models/Battle";
import { Character } from "../../../../../domain/models/Character";

export type FightState = {
  turn: number;
  challenger: {
    character: Character;
    damageTaken: number;
  };
  opponent: {
    character: Character;
    damageTaken: number;
  };
  events: BattleEvent[];
};

const defaultCharacter: Character = {
  id: "",
  name: "",
  rank: 0,
  skillPoints: 0,
  healthPoints: 0,
  attack: 0,
  defense: 0,
  magik: 0,
};

export const defaultFightState: FightState = {
  turn: 0,
  challenger: {
    character: defaultCharacter,
    damageTaken: 0,
  },
  opponent: {
    character: defaultCharacter,
    damageTaken: 0,
  },
  events: [],
};

export function fightReducer(
  state: FightState,
  event: BattleEvent
): FightState {
  switch (event.type) {
    case "START":
      return {
        ...state,
        challenger: {
          character: event.challenger,
          damageTaken: 0,
        },
        opponent: {
          character: event.opponent,
          damageTaken: 0,
        },
        events: state.events.concat(event),
      };

    case "NEW_TURN":
      return {
        ...state,
        turn: state.turn + 1,
        events: state.events.concat(event),
      };
    case "CHALLENGER_ROLL":
      return {
        ...state,
        events: state.events.concat(event),
      };
    case "CHALLENGER_ATTACK":
      let challengerDamage = 0;
      switch (event.result.type) {
        case "CRITICAL":
        case "SUCCESS":
          challengerDamage = event.result.damage;
          break;
      }
      return {
        ...state,
        opponent: {
          ...state.opponent,
          damageTaken: state.opponent.damageTaken + challengerDamage,
        },
        events: state.events.concat(event),
      };
    case "OPPONENT_ROLL":
      return {
        ...state,
        events: state.events.concat(event),
      };
    case "OPPONENT_ATTACK":
      let opponentDamage = 0;
      switch (event.result.type) {
        case "CRITICAL":
        case "SUCCESS":
          opponentDamage = event.result.damage;
          break;
      }
      return {
        ...state,
        challenger: {
          ...state.challenger,
          damageTaken: state.challenger.damageTaken + opponentDamage,
        },
        events: state.events.concat(event),
      };
    case "END":
      return {
        ...state,
        events: state.events.concat(event),
      };
    default:
      return state;
  }
}
