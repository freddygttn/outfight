import React from "react";
import ReactDOM from "react-dom";
import { Battle } from "../../../../../domain/models/Battle";
import FightView from "./FightView";

it("renders without crashing", () => {
  const battle: Battle = {
    events: [],
  };
  const div = document.createElement("div");
  ReactDOM.render(<FightView battle={battle} />, div);
});
