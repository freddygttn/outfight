import classNames from "classnames";
import React, { useEffect, useState } from "react";
import { from, interval, Subscription } from "rxjs";
import { concatMap, map, scan, take } from "rxjs/operators";
import { Battle, BattleEvent } from "../../../../../domain/models/Battle";
import Button from "../../../../base-components/button/Button";
import CharacterInfo from "../character-info/CharacterInfo";
import FightLog from "../fight-log/FightLog";
import { defaultFightState, fightReducer, FightState } from "./FightReducer";

export type FightViewProps = {
  className?: string;
  battle: Battle;
  onLeave?: () => void;
};

export function delayForEvent(event: BattleEvent): number {
  switch (event.type) {
    case "START":
      return 0;
    case "NEW_TURN":
      return 1000;
    case "CHALLENGER_ROLL":
    case "OPPONENT_ROLL":
      return 800;
    case "CHALLENGER_ATTACK":
    case "OPPONENT_ATTACK":
      return 2500;
    case "END":
      return 1000;
    default:
      return 1000;
  }
}

const FightView = ({
  className,
  battle,
  onLeave = () => {},
}: FightViewProps) => {
  const [fightState, setFightState] = useState<FightState>(defaultFightState);
  const [shouldSkip, setShouldSkip] = useState(false);
  const hasEnded =
    fightState.events.length > 0 &&
    fightState.events[fightState.events.length - 1].type === "END";

  // Fight control
  useEffect(() => {
    let subscription: Subscription | undefined;

    if (shouldSkip) {
      const lastState = battle.events.reduce(fightReducer, defaultFightState);
      setFightState(lastState);
    } else {
      subscription = from(battle.events)
        .pipe(
          concatMap((item) =>
            interval(delayForEvent(item)).pipe(
              take(1),
              map(() => item)
            )
          ),
          scan(fightReducer, defaultFightState)
        )
        .subscribe({ next: setFightState });
    }

    return () => {
      subscription?.unsubscribe();
    };
  }, [battle, shouldSkip]);

  // Go back to ready state
  useEffect(() => {
    return onLeave;
  }, [onLeave]);

  return (
    <div className={classNames(className)}>
      <div className="mx-8 mt-8 flex items-center justify-between content-center">
        <CharacterInfo
          className="max-w-1/3"
          character={fightState.challenger.character}
          damageTaken={fightState.challenger.damageTaken}
        />
        <p className="text-4xl italic">VS</p>
        <CharacterInfo
          className="max-w-1/3"
          character={fightState.opponent.character}
          damageTaken={fightState.opponent.damageTaken}
        />
      </div>
      <div className="mt-8 mx-8 text-right">
        {hasEnded && (
          <Button size="lg" variant="primary" onClick={onLeave}>
            Back to the arena
          </Button>
        )}
        <Button
          size="sm"
          onClick={() => setShouldSkip(true)}
          disabled={shouldSkip || hasEnded}
        >
          Skip
        </Button>
      </div>

      <FightLog className="mt-2 mx-8" events={fightState.events} />
    </div>
  );
};

export default FightView;
