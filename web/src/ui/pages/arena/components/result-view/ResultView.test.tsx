import React from "react";
import ReactDOM from "react-dom";
import ResultView from "./ResultView";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ResultView results={[]} />, div);
});
