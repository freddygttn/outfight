import classNames from "classnames";
import React from "react";
import { formatDistance } from "date-fns";

import { BattleResult } from "../../../../../domain/models/BattleResult";

export type ResultViewProps = {
  className?: string;
  results: BattleResult[];
};

function formatOutcome(result: BattleResult) {
  switch (result.outcome) {
    case "VICTORY":
      return <span className="text-yellow-400">Victory</span>;

    case "DRAW":
      return <span className="text-purple-600">Draw</span>;

    case "DEFEAT":
      return <span className="text-red-600">Defeat</span>;
    default:
      return null;
  }
}

const ResultView = ({ className, results }: ResultViewProps) => {
  return (
    <div className={classNames("", className)}>
      {results.length > 0 ? (
        <>
          <h3 className="text-xl text-center">Last results</h3>

          <table className="mt-2 min-w-full table-auto bg-gray-700 rounded-md">
            <tbody className="text-left">
              {results.map((result, index) => (
                <tr
                  key={`result-${result.id}`}
                  className={classNames("border-gray-600", {
                    "border-t": index > 0,
                  })}
                >
                  <td className="text-gray-200 px-4 py-2">
                    {formatDistance(new Date(result.resolvedAt), new Date(), {
                      addSuffix: true,
                    })}
                  </td>
                  <td className="px-4 py-2">{formatOutcome(result)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      ) : (
        <p className="text-center">This character hasn't fought yet.</p>
      )}
    </div>
  );
};

export default ResultView;
