import React from "react";
import ReactDOM from "react-dom";
import CharacterInfo from "./CharacterInfo";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<CharacterInfo damageTaken={0} />, div);
});
