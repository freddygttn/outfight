import classNames from "classnames";
import React from "react";
import { Character } from "../../../../../domain/models/Character";

export type CharacterInfoProps = {
  className?: string;
  character?: Character;
  damageTaken: number;
};

const CharacterInfo = ({
  className,
  character,
  damageTaken,
}: CharacterInfoProps) => {
  if (character === undefined) {
    return null;
  }

  return (
    <div className={classNames("bg-yellow-900 rounded p-2", className)}>
      <p className="text-lg truncate">
        {character ? <>{character.name}</> : null}
      </p>
      <p className="text-sm">{character ? <>Rank {character.rank}</> : null}</p>
      <p>
        HP:{" "}
        {character ? (
          <>
            {character.healthPoints - damageTaken} / {character.healthPoints}
          </>
        ) : null}
      </p>
      <p className="text-sm text-gray-300">
        A: {character.attack} D: {character.defense} M: {character.magik}
      </p>
    </div>
  );
};

export default CharacterInfo;
