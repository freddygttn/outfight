import { Link, RouteComponentProps } from "@reach/router";
import React, { useState } from "react";
import { useSessionHook } from "../../../domain/sessions";
import Button from "../../base-components/button/Button";
import { routes } from "../routes";

const Register = (_props: RouteComponentProps) => {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [{ isLoading, currentError }, service] = useSessionHook();
  const isInvalid =
    email.trim().length === 0 ||
    !email.match(/^[^\s]+@[^\s]+$/) ||
    username.trim().length === 0 ||
    password.length < 8;

  const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    service.register(email, password, username);
  };

  return (
    <div className="px-8">
      <h2 className="mt-8 inline-block text-3xl borber-0 border-b-4 border-yellow-400">
        Register
      </h2>
      <form className="mt-8 p-8 bg-gray-700" onSubmit={handleFormSubmit}>
        <label className="block">
          <span className="">E-mail address</span>
          <input
            className="mt-0 block w-full px-1 bg-transparent border-0 border-b-2 border-gray-900 focus:ring-0 focus:border-yellow-400"
            type="text"
            defaultValue={email}
            onChange={(e) => setEmail(e.currentTarget.value)}
          ></input>
        </label>
        <label className="block mt-8">
          <span className="">Password (&gt;= 8 chars.)</span>
          <input
            className="mt-0 block w-full px-1 bg-transparent border-0 border-b-2 border-gray-900 focus:ring-0 focus:border-yellow-400"
            type="password"
            defaultValue={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
          ></input>
        </label>
        <label className="block mt-8">
          <span className="">Username</span>
          <input
            className="mt-0 block w-full px-1 bg-transparent border-0 border-b-2 border-gray-900 focus:ring-0 focus:border-yellow-400"
            type="text"
            defaultValue={username}
            onChange={(e) => setUsername(e.currentTarget.value)}
          ></input>
        </label>
        <div className="mt-8 text-center">
          <Button
            type="submit"
            disabled={isLoading || isInvalid}
            variant="primary"
          >
            Create an account
          </Button>
          {currentError !== null ? (
            <p className="mt-4">{currentError}</p>
          ) : null}
          <Link className="block mt-4" to={routes.signIn()}>
            Already have an account? Sign in
          </Link>
        </div>
      </form>
    </div>
  );
};

export default Register;
