import { Link, RouteComponentProps } from "@reach/router";
import React, { useState } from "react";
import { useSessionHook } from "../../../domain/sessions";
import { getAppErrorMessage } from "../../../domain/models/AppError";
import { routes } from "../routes";
import Button from "../../base-components/button/Button";

const SignIn = (_: RouteComponentProps) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [{ isLoading, currentError }, service] = useSessionHook();
  const isValid = email.trim().length > 0 && password.length > 0;

  const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    service.signIn(email, password);
  };

  return (
    <div className="px-8">
      <h2 className="mt-8 inline-block text-3xl borber-0 border-b-4 border-yellow-400">
        Sign in
      </h2>
      <form className="mt-8 p-8 bg-gray-700" onSubmit={handleFormSubmit}>
        <label className="block">
          <span className="">E-mail address</span>
          <input
            className="mt-0 block w-full px-1 bg-transparent border-0 border-b-2 border-gray-900 focus:ring-0 focus:border-yellow-400"
            type="text"
            defaultValue={email}
            onChange={(e) => setEmail(e.currentTarget.value)}
          ></input>
        </label>
        <label className="block mt-8">
          <span className="">Password</span>
          <input
            className="mt-0 block w-full px-1 bg-transparent border-0 border-b-2 border-gray-900 focus:ring-0 focus:border-yellow-400"
            type="password"
            defaultValue={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
          ></input>
        </label>
        <div className="mt-8 text-center">
          <Button
            type="submit"
            disabled={isLoading || !isValid}
            variant="primary"
          >
            Sign in
          </Button>
          {currentError !== null ? (
            <p className="mt-4">{getAppErrorMessage(currentError)}</p>
          ) : null}
          <Link className="block mt-4" to={routes.register()}>
            No account yet? Register
          </Link>
        </div>
      </form>
    </div>
  );
};

export default SignIn;
