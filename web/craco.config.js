module.exports = {
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
  devServer: (devServerConfig, { _env, _paths, proxy, _allowedHost }) => {
    if (proxy[0] && proxy[0].target && process.env.PROXY) {
      proxy[0].target = process.env.PROXY;
    }
    return devServerConfig;
  },
};
