# Outfight

Outfight is a web game where you can train your characters by fighting into an Arena.

## About

The frontend is made with Typescript and React.
- TailwindCSS for easy styling
- Akita for state management

The backend is made with Elixir and Phoenix.
- Guardian for auth
- Ecto as SQL DSL

Data is stored in a PostgreSQL database.

## Set up

`docker compose` is used to easily set up the app, therefore `docker` must be installed.

> `docker-compose` can be used instead if the `docker` version is too old

Let's start by building the containers:

```shell
docker compose build
```

Then, we need to create the database and run the migrations.

```shell
docker compose run --rm api mix ecto.setup
```

> The database can be populated with some data. Each time the following command is run, it will generate 100 new characters.
>
>```shell
>docker compose run --rm api mix api.gen.data
> ```

## Start

```shell
docker compose up
```

These messages should confirm that everything is good.

```shell
db_1   | 2021-05-27 13:55:11.854 UTC [1] LOG:  database system is ready to accept connections
...
api_1  | [info] Access ApiWeb.Endpoint at http://localhost:4000
...
web_1  | Starting the development server...
```

App can be accessed at [http://localhost:3000](http://localhost:3000).

Server telemetry is available at [http://localhost:4000/dashboard](http://localhost:4000/dashboard).

## Stop

```shell
docker compose down
```

## Test

Coverage is far from 100%. Mostly smoke tests for the frontend.

Tests may be launched with:

```shell
docker compose run --rm -e MIX_ENV=test api mix test
docker compose run --rm -e web yarn test
```

## Alternative

If using Docker is an issue, everything can be installed by following each `README.md` within `web` and `api` directories:

- [web](./web/README.md)
- [api](./api/README.md)

## Todo

- Add storyboard
- More tests
- Better UI
- Animated battle
