defmodule Api.Repo.Migrations.CreateCharacters do
  use Ecto.Migration

  def change do
    create table(:characters) do
      add :name, :string, null: false
      add :rank, :integer, null: false
      add :skill_points_earned, :integer, null: false
      add :health_points, :integer, null: false
      add :attack, :integer, null: false
      add :defense, :integer, null: false
      add :magik, :integer, null: false
      add :player_id, references(:users, on_delete: :nilify_all)

      timestamps()
    end

    create index(:characters, [:player_id])
  end
end
