defmodule Api.Repo.Migrations.CreateBattleResults do
  use Ecto.Migration

  def change do
    create table(:battle_results) do
      add :outcome, :string, null: false
      add :resolved_at, :utc_datetime, null: false
      add :challenger_id, references(:characters, on_delete: :delete_all), null: false
      add :opponent_id, references(:characters, on_delete: :nilify_all)

      timestamps()
    end

    create index(:battle_results, [:challenger_id])
    create index(:battle_results, [:opponent_id])
  end
end
