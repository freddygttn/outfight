# Api

`Elixir 1.12` must be installed.

- [Official documentation](https://elixir-lang.org/install.html)
- [kiex](https://github.com/taylor/kiex)
- [asdf](https://github.com/asdf-vm/asdf)

PostgreSQL database can be configured in `.env` file.

To start your Phoenix server:

- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server`

## TODO

- Corsica could be added for CORS management
- Controllers tests
- Maybe some p-tests for sensitive parts
