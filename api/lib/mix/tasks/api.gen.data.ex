defmodule Mix.Tasks.Api.Gen.Data do
  use Mix.Task

  import Api.Factory

  @shortdoc "Generates fake Users and Characters"
  def run(_args) do
    Mix.Task.run("app.start")
    Mix.shell().info("Generating...")

    password = Argon2.hash_pwd_salt(Faker.String.base64())

    Logger.remove_backend(:console)

    1..10
    |> Enum.to_list()
    |> Enum.each(fn _ ->
      user = insert(:user, hashed_password: password)
      generate_characters(user)
    end)

    Mix.shell().info("Done.")
  end

  defp generate_characters(player) do
    1..10
    |> Enum.to_list()
    |> Enum.each(fn _ ->
      insert(:character, player: player)
    end)
  end
end
