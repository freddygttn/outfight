defmodule ApiWeb.AuthView do
  use ApiWeb, :view
  alias ApiWeb.AuthView

  def render("show.json", %{auth: auth}) do
    %{data: render_one(auth, AuthView, "auth.json")}
  end

  def render("auth.json", %{auth: auth}) do
    %{id: auth.id |> Api.Hashids.encode(), email: auth.email, username: auth.username}
  end
end
