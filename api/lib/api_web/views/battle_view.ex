defmodule ApiWeb.BattleView do
  use ApiWeb, :view
  alias ApiWeb.BattleView
  alias ApiWeb.BattleResultView
  alias ApiWeb.CharacterView
  alias Api.Arena.BattleResult
  alias Api.Arena.Character

  def render("show.json", %{battle: battle}) do
    %{data: render_one(battle, BattleView, "battle.json")}
  end

  def render("battle.json", %{battle: battle}) do
    %{
      updated_character: render_character(battle.updated_character),
      result: render_battle_result(battle.battle_result),
      events: battle.battle.events |> Enum.map(&render_event/1) |> Enum.reverse()
    }
  end

  defp render_event({:start, %Character{} = challenger, %Character{} = opponent}),
    do: %{
      type: "START",
      challenger: render_character(challenger),
      opponent: render_character(opponent)
    }

  defp render_event({:new_turn, count}), do: %{type: "NEW_TURN", count: count}
  defp render_event({:challenger_roll, value}), do: %{type: "CHALLENGER_ROLL", value: value}

  defp render_event({:challenger_attack, result}),
    do: %{type: "CHALLENGER_ATTACK", result: render_attack_result(result)}

  defp render_event({:opponent_roll, value}), do: %{type: "OPPONENT_ROLL", value: value}

  defp render_event({:opponent_attack, result}),
    do: %{type: "OPPONENT_ATTACK", result: render_attack_result(result)}

  defp render_event({:end, outcome}), do: %{type: "END", outcome: render_outcome(outcome)}

  defp render_attack_result({:critical, damage}), do: %{type: "CRITICAL", damage: damage}
  defp render_attack_result({:success, damage}), do: %{type: "SUCCESS", damage: damage}
  defp render_attack_result(:failure), do: %{type: "FAILURE"}

  defp render_outcome(:victory), do: "VICTORY"
  defp render_outcome(:draw), do: "DRAW"
  defp render_outcome(:defeat), do: "DEFEAT"

  defp render_character(%Character{} = character) do
    render_one(character, CharacterView, "character.json")
  end

  defp render_battle_result(%BattleResult{} = battle_result) do
    render_one(battle_result, BattleResultView, "battle_result.json")
  end
end
