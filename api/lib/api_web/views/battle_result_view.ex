defmodule ApiWeb.BattleResultView do
  use ApiWeb, :view
  alias ApiWeb.BattleResultView

  def render("index.json", %{battle_results: battle_results}) do
    %{data: render_many(battle_results, BattleResultView, "battle_result.json")}
  end

  def render("show.json", %{battle_result: battle_result}) do
    %{data: render_one(battle_result, BattleResultView, "battle_result.json")}
  end

  def render("battle_result.json", %{battle_result: battle_result}) do
    %{
      id: battle_result.id |> Api.Hashids.encode(),
      outcome: render_outcome(battle_result.outcome),
      resolved_at: battle_result.resolved_at
    }
  end

  defp render_outcome(:victory), do: "VICTORY"
  defp render_outcome(:draw), do: "DRAW"
  defp render_outcome(:defeat), do: "DEFEAT"
end
