defmodule ApiWeb.CharacterView do
  use ApiWeb, :view
  alias ApiWeb.CharacterView

  def render("index.json", %{characters: characters}) do
    %{data: render_many(characters, CharacterView, "character.json")}
  end

  def render("show.json", %{character: character}) do
    %{data: render_one(character, CharacterView, "character.json")}
  end

  def render("character.json", %{character: character}) do
    total_spent =
      Api.Arena.CharacterValidator.total_skill_cost(0, character.attack) +
        Api.Arena.CharacterValidator.total_skill_cost(0, character.defense) +
        Api.Arena.CharacterValidator.total_skill_cost(0, character.magik) +
        character.health_points - 10

    %{
      id: character.id |> Api.Hashids.encode(),
      name: character.name,
      rank: character.rank,
      skill_points: character.skill_points_earned - total_spent,
      health_points: character.health_points,
      attack: character.attack,
      defense: character.defense,
      magik: character.magik
    }
  end
end
