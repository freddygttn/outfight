defmodule ApiWeb.AuthMaybePipeline do
  use Guardian.Plug.Pipeline, otp_app: :api

  plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}
  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  plug Guardian.Plug.LoadResource, allow_blank: true
end
