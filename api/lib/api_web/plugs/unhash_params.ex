defmodule ApiWeb.Plugs.UnhashParams do
  @moduledoc """
  Transforms Hashids parameters to their initial integer value.
  The param_names option is the list of parameters to decode.

  It can be used inside a controller as such :
  `plug ApiWeb.Plugs.UnhashParams, param_names: ["character_id"]`
  """
  @behaviour Plug

  @impl Plug
  @spec init(opts :: Keyword.t()) :: Keyword.t()
  def init(opts), do: opts

  @impl Plug
  @spec call(conn :: Plug.Conn.t(), opts :: Keyword.t()) :: Plug.Conn.t()
  def call(conn, opts) do
    param_names = opts |> Keyword.get(:param_names, [])
    %Plug.Conn{conn | params: unhash_params(conn.params, param_names)}
  end

  defp unhash_params(params, []), do: params

  defp unhash_params(params, [param_name | remaining]) do
    params
    |> Map.update(param_name, nil, &unhash_value/1)
    |> unhash_params(remaining)
  end

  defp unhash_value(value) when is_binary(value) do
    {:ok, [unhashed]} = Api.Hashids.decode(value)
    unhashed
  end

  defp unhash_value(value), do: value
end
