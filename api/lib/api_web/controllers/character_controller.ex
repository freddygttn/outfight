defmodule ApiWeb.CharacterController do
  use ApiWeb, :controller

  plug ApiWeb.Plugs.UnhashParams, param_names: ["id"]

  alias Api.Arena
  alias Api.Arena.Character

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    %{id: player_id} = Api.Guardian.Plug.current_resource(conn)

    characters = Arena.list_player_characters(player_id)
    render(conn, "index.json", characters: characters)
  end

  def create(conn, %{"character" => character_params}) do
    %{id: player_id} = Api.Guardian.Plug.current_resource(conn)
    create_params = character_params |> Map.put("player_id", player_id)

    with {:ok, _count} <- Arena.available_character_slots(player_id),
         {:ok, %Character{} = character} <- Arena.create_character(create_params) do
      conn
      |> put_status(:created)
      |> render("show.json", character: character)
    else
      {:error, :limit_reached} -> {:error, :forbidden}
      err -> err
    end
  end

  def show(conn, %{"id" => id}) do
    %{id: player_id} = Api.Guardian.Plug.current_resource(conn)
    character = Arena.get_player_character!(player_id, id)

    render(conn, "show.json", character: character)
  end

  def update(conn, %{"id" => id, "character" => character_params}) do
    %{id: player_id} = Api.Guardian.Plug.current_resource(conn)
    character = Arena.get_player_character!(player_id, id)

    with {:ok, %Character{} = updated_character} <-
           Arena.update_character_attributes(character, character_params) do
      render(conn, "show.json", character: updated_character)
    end
  end

  def delete(conn, %{"id" => id}) do
    character = Arena.get_character!(id)

    with {:ok, %Character{}} <- Arena.delete_character(character) do
      send_resp(conn, :no_content, "")
    end
  end
end
