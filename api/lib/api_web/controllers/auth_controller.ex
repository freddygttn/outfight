defmodule ApiWeb.AuthController do
  use ApiWeb, :controller

  alias Api.Accounts

  action_fallback ApiWeb.FallbackController

  def me(conn, _) do
    user = Api.Guardian.Plug.current_resource(conn)

    conn
    |> render_me(user)
  end

  defp render_me(_conn, nil) do
    {:error, :not_found}
  end

  defp render_me(conn, user) do
    conn
    |> put_status(:ok)
    |> render("show.json", auth: user)
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    with {:ok, user} <- Accounts.authenticate_user(%{email: email, password: password}) do
      conn
      |> Api.Guardian.Plug.sign_in(user)
      |> put_status(:ok)
      |> render("show.json", auth: user)
    end
  end

  def register(conn, %{"user" => user_params}) do
    with {:ok, user} <- Accounts.register_user(user_params) do
      conn
      |> Api.Guardian.Plug.sign_in(user)
      |> put_status(:created)
      |> render("show.json", auth: user)
    end
  end

  def sign_out(conn, _) do
    conn
    |> Api.Guardian.Plug.sign_out()
    |> put_status(:ok)
    |> json(%{})
  end
end
