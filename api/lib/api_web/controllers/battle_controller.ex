defmodule ApiWeb.BattleController do
  use ApiWeb, :controller

  plug ApiWeb.Plugs.UnhashParams, param_names: ["character_id"]

  alias Api.Arena

  action_fallback ApiWeb.FallbackController

  def create(conn, %{"character_id" => character_id}) do
    with true <- Arena.is_character_able_to_fight?(character_id),
         character <- Arena.get_character!(character_id),
         opponents_with_rank <- Arena.Matchmaker.list_battle_opponents(character),
         {:ok, closest_opponents} <-
           Arena.Matchmaker.narrow_opponents_by_rank(opponents_with_rank),
         opponents_with_fight_count <-
           Arena.Matchmaker.list_fight_counts_against_opponents(character, closest_opponents),
         {:ok, lowest_fights_opponents} <-
           Arena.Matchmaker.narrow_opponents_by_fight_count(opponents_with_fight_count),
         {:ok, opponent} <- Arena.Matchmaker.get_random_opponent(lowest_fights_opponents) do
      battle = Arena.BattleResolver.resolve_battle(character, opponent)

      outcome = Arena.Battle.outcome(battle)

      # Malus / Bonus
      {:ok, new_character} =
        case outcome do
          :victory ->
            Arena.update_character_rank(character, %{
              rank: character.rank + 1,
              skill_points_earned: character.skill_points_earned + 1
            })

          :draw ->
            {:ok, character}

          :defeat ->
            Arena.update_character_rank(character, %{
              rank: Kernel.max(character.rank - 1, 1)
            })
        end

      {:ok, battle_result} =
        Arena.create_battle_result(%{
          outcome: outcome,
          resolved_at: DateTime.utc_now(),
          challenger_id: character.id,
          opponent_id: opponent.id
        })

      render(conn, "show.json",
        battle: %{
          battle: battle,
          battle_result: battle_result,
          updated_character: new_character
        }
      )
    else
      {:error, :no_opponent} -> {:error, :conflict}
      err -> err
    end
  end
end
