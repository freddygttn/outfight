defmodule ApiWeb.BattleResultController do
  use ApiWeb, :controller

  plug ApiWeb.Plugs.UnhashParams, param_names: ["character_id"]

  alias Api.Arena

  action_fallback ApiWeb.FallbackController

  def index(conn, %{"character_id" => character_id}) do
    battle_results = Arena.list_character_battle_results(character_id)
    render(conn, "index.json", battle_results: battle_results)
  end
end
