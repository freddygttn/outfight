defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :maybe do
    plug ApiWeb.AuthMaybePipeline
  end

  pipeline :authenticated do
    plug ApiWeb.AuthAccessPipeline
  end

  scope "/api/v1/auth", ApiWeb do
    pipe_through [:api]

    post "/sign_in", AuthController, :sign_in
    post "/register", AuthController, :register
  end

  scope "/api/v1/auth", ApiWeb do
    pipe_through [:api, :maybe]

    get "/me", AuthController, :me
    post "/sign_out", AuthController, :sign_out
  end

  scope "/api/v1", ApiWeb do
    pipe_through [:api, :authenticated]

    get "/characters", CharacterController, :index
    get "/characters/:id", CharacterController, :show
    post "/characters", CharacterController, :create
    put "/characters/:id", CharacterController, :update
    delete "/characters/:id", CharacterController, :delete
    get "/characters/:character_id/battle_results", BattleResultController, :index
    post "/characters/:character_id/battles", BattleController, :create
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: ApiWeb.Telemetry
    end
  end
end
