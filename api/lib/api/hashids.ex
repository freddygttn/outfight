defmodule Api.Hashids do
  @coder Hashids.new(
           salt: "Outsalt... I mean, Outfight!",
           min_len: 5
         )

  def encode(ids) do
    Hashids.encode(@coder, ids)
  end

  def decode(data) do
    Hashids.decode(@coder, data)
  end
end
