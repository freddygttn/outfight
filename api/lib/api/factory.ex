defmodule Api.Factory do
  @moduledoc """
  Contains factories to generate fake data
  """
  use ExMachina.Ecto, repo: Api.Repo

  alias Api.Accounts.User
  alias Api.Arena.BattleResult
  alias Api.Arena.Character

  # ---------------------------------------------------------------------------
  # Users
  # ---------------------------------------------------------------------------
  def user_factory do
    password = Faker.String.base64()

    %User{
      email: Faker.Internet.safe_email(),
      hashed_password: "Argon2.hash_pwd_salt(#{password})",
      username: Faker.Internet.user_name()
    }
  end

  # ---------------------------------------------------------------------------
  # Character
  # ---------------------------------------------------------------------------
  def character_factory do
    # 1 =< X =< N
    rank = :rand.uniform(100)

    power_level =
      (0.4 * (rank - 1))
      |> :math.sqrt()

    min = power_level |> Float.floor() |> Kernel.trunc()
    max = (5 * power_level) |> Float.floor() |> Kernel.trunc()

    hp = Faker.random_between(min, max) + 10
    attack = Faker.random_between(min, max)
    defense = Faker.random_between(min, max)
    magik = Faker.random_between(min, max)

    total =
      Api.Arena.CharacterValidator.total_skill_cost(0, attack) +
        Api.Arena.CharacterValidator.total_skill_cost(0, defense) +
        Api.Arena.CharacterValidator.total_skill_cost(0, magik) + hp - 10

    %Character{
      name: Faker.Superhero.En.name(),
      rank: rank,
      health_points: hp,
      attack: attack,
      defense: defense,
      magik: magik,
      skill_points_earned: total,
      player: build(:user)
    }
  end

  # ---------------------------------------------------------------------------
  # Battle Results
  # ---------------------------------------------------------------------------
  def battle_result_factory do
    %BattleResult{
      outcome: sequence(:outcome, [:victory, :defeat, :draw]),
      resolved_at: Faker.DateTime.backward(10),
      challenger: build(:character),
      opponent: build(:character)
    }
  end
end
