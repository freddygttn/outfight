defmodule Api.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Accounts.User

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Authenticates a user.

  ## Examples

      iex> authenticate_user(%{email: "foo@example.com", password: "correct_password"})
      {:ok, %User{}}

      iex> authenticate_user(%{email: "foo@example.com", password: "invalid_password"})
      {:error, :unauthorized}

  """
  def authenticate_user(%{email: email, password: password})
      when is_binary(email) and is_binary(password) do
    user = Repo.get_by(User, email: email)

    if User.valid_password?(user, password),
      do: {:ok, user},
      else: {:error, :unauthorized}
  end

  def authenticate_user(_), do: {:error, :unauthorized}

  @doc """
  Registers a user.

  ## Examples

      iex> register_user(%{field: value})
      {:ok, %User{}}

      iex> register_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def register_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end
end
