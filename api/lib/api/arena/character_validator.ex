defmodule Api.Arena.CharacterValidator do
  @moduledoc """
  Provides validation functions for Characters.
  """

  alias Api.Arena.Character

  @spec validate_skill_points(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  def validate_skill_points(
        %Ecto.Changeset{
          data: %Character{
            skill_points_earned: sp
          },
          changes: %{
            health_points: new_hp,
            attack: new_atk,
            defense: new_def,
            magik: new_mgk
          }
        } = changeset
      ) do
    atk_cost = total_skill_cost(0, new_atk)
    def_cost = total_skill_cost(0, new_def)
    mgk_cost = total_skill_cost(0, new_mgk)
    hp_cost = new_hp - 10

    total_cost = atk_cost + def_cost + mgk_cost + hp_cost

    if total_cost <= sp do
      changeset
    else
      changeset
      |> Ecto.Changeset.add_error(
        :not_enough_sp,
        "not enough skill points : expected #{sp} or less, got #{total_cost}"
      )
    end
  end

  def validate_skill_points(%Ecto.Changeset{} = changeset), do: changeset

  @doc """
  Computes the cost to increment a skill (attack, defense or magik).

  ## Examples

  iex> Api.Arena.CharacterValidator.skill_cost(5)
  1

  iex> Api.Arena.CharacterValidator.skill_cost(12)
  3

  """
  @spec skill_cost(integer) :: integer
  def skill_cost(level) when level > 1 do
    ceil((level - 1) / 5)
  end

  def skill_cost(1), do: 1

  @doc """
  Computes the total cost to increase a skill (attack, defense or magik) from
  a given value to another.

  ## Examples

      iex> Api.Arena.CharacterValidator.total_skill_cost(3,7)
      5

      iex> Api.Arena.CharacterValidator.total_skill_cost(10,14)
      11

  """
  @spec total_skill_cost(integer, integer) :: integer
  def total_skill_cost(from, to) when to - from > 0 do
    levels =
      for n <- (from + 1)..to,
          do: n

    levels
    |> Enum.map(&skill_cost/1)
    |> Enum.reduce(fn n, acc -> n + acc end)
  end

  def total_skill_cost(from, from), do: 0
end
