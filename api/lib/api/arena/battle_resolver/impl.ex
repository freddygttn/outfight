defmodule Api.Arena.BattleResolver.Impl do
  @moduledoc """
  Implementation for the arena core logic.
  """
  alias Api.Arena.Character, as: Character
  alias Api.Arena.Battle, as: Battle

  @doc """
  See `Arena` interface.

  Initializes the `Battle` and start `resolve_turns` loop.
  """
  @spec resolve_battle(Character.t(), Character.t()) :: Battle.t()
  def resolve_battle(challenger, opponent) do
    %Battle{
      challenger: challenger,
      opponent: opponent,
      events: [{:start, challenger, opponent}]
    }
    |> resolve_next_turn()
  end

  @doc """
  Performs turn actions.
  Calls itself at the end to auto-turn the whole battle.
  Stops when battle has ended.

  Returns a resolved turn.
  """
  @spec resolve_next_turn(Battle.t()) :: Battle.t()
  def resolve_next_turn(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def resolve_next_turn(battle) do
    battle
    |> new_turn()
    |> challenger_roll()
    |> attack()
    |> apply_attack()
    |> check_for_end()
    |> opponent_roll()
    |> attack()
    |> apply_attack()
    |> check_for_end()
    |> resolve_next_turn()
  end

  @doc """
  Initializes a new turn.
  """
  @spec new_turn(Battle.t()) :: Battle.t()
  def new_turn(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def new_turn(%Battle{turn_count: count, events: events} = battle) do
    new_count = count + 1
    %Battle{battle | turn_count: new_count, events: [{:new_turn, new_count} | events]}
  end

  @doc """
  Rolls the dice for the challenger.
  """
  @spec challenger_roll(Battle.t()) :: Battle.t()
  def challenger_roll(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def challenger_roll(%Battle{challenger: %Character{attack: atk}, events: events} = battle) do
    value = dice_roll(atk)
    %Battle{battle | events: [{:challenger_roll, value} | events]}
  end

  @doc """
  Rolls the dice for the opponent.
  """
  @spec opponent_roll(Battle.t()) :: Battle.t()
  def opponent_roll(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def opponent_roll(%Battle{opponent: %Character{attack: atk}, events: events} = battle) do
    value = dice_roll(atk)
    %Battle{battle | events: [{:opponent_roll, value} | events]}
  end

  # Performs a dice roll.
  @spec dice_roll(integer()) :: integer()
  defp dice_roll(0), do: 0
  defp dice_roll(n), do: :rand.uniform(n)

  @doc """
  Applies the attack rules logic given a previous roll event.
  """
  @spec attack(Battle.t()) :: Battle.t()
  def attack(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def attack(
        %Battle{
          challenger: %Character{magik: mgk},
          opponent: %Character{defense: defense},
          events: [{:challenger_roll, roll} | _] = events
        } = battle
      ) do
    attack_event = {:challenger_attack, compute_attack_status(roll, mgk, defense)}
    %Battle{battle | events: [attack_event | events]}
  end

  def attack(
        %Battle{
          challenger: %Character{defense: defense},
          opponent: %Character{magik: mgk},
          events: [{:opponent_roll, roll} | _] = events
        } = battle
      ) do
    attack_event = {:opponent_attack, compute_attack_status(roll, mgk, defense)}
    %Battle{battle | events: [attack_event | events]}
  end

  # Resolves the attack status.
  @type attack_status :: {:critical, integer} | {:success, integer} | :failure
  @spec compute_attack_status(integer(), integer(), integer()) :: attack_status()
  defp compute_attack_status(roll, magik, defense)
       when roll > defense and roll - defense === magik do
    {:critical, roll - defense + magik}
  end

  defp compute_attack_status(roll, _, defense)
       when roll > defense do
    {:success, roll - defense}
  end

  defp compute_attack_status(_, _, _) do
    :failure
  end

  @doc """
  Apply the attack result to the battle.
  """
  @spec apply_attack(Battle.t()) :: Battle.t()
  def apply_attack(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def apply_attack(
        %Battle{
          total_challenger_damage: total,
          events: [{:challenger_attack, {_, damage}} | _]
        } = battle
      ) do
    %Battle{battle | total_challenger_damage: total + damage}
  end

  def apply_attack(
        %Battle{
          total_opponent_damage: total,
          events: [{:opponent_attack, {_, damage}} | _]
        } = battle
      ) do
    %Battle{battle | total_opponent_damage: total + damage}
  end

  def apply_attack(battle), do: battle

  @doc """
  Checks if the game has come to an end.
  """
  @spec check_for_end(Battle.t()) :: Battle.t()
  def check_for_end(%Battle{events: [{:end, _} | _]} = battle), do: battle

  def check_for_end(
        %Battle{
          turn_count: turn,
          challenger: %Character{attack: p_atk, defense: p_def},
          opponent: %Character{attack: o_atk, defense: o_def},
          events: events
        } = battle
      )
      when turn >= 3 and p_def >= o_atk and o_def >= p_atk do
    %Battle{battle | events: [{:end, :draw} | events]}
  end

  def check_for_end(
        %Battle{
          turn_count: turn,
          total_challenger_damage: 0,
          total_opponent_damage: 0,
          events: events
        } = battle
      )
      when turn >= 5 do
    %Battle{battle | events: [{:end, :draw} | events]}
  end

  def check_for_end(
        %Battle{
          total_challenger_damage: total,
          opponent: %Character{health_points: hp},
          events: events
        } = battle
      )
      when total >= hp do
    %Battle{battle | events: [{:end, :victory} | events]}
  end

  def check_for_end(
        %Battle{
          total_opponent_damage: total,
          challenger: %Character{health_points: hp},
          events: events
        } = battle
      )
      when total >= hp do
    %Battle{battle | events: [{:end, :defeat} | events]}
  end

  def check_for_end(battle), do: battle
end
