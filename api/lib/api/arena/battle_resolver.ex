defmodule Api.Arena.BattleResolver do
  @moduledoc """
  Interface for the arena battle core logic.
  """

  @doc """
  Resolve an entire battle between two characters.
  `challenger` is the character owned by the player that launched the battle.
  `opponent` is the character chosen by the matchmaking system.
  """
  defdelegate resolve_battle(challenger, opponent), to: Api.Arena.BattleResolver.Impl
end
