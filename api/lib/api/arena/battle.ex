defmodule Api.Arena.Battle do
  @moduledoc """
  Describes a battle in the Arena.

  A battle is an exchange of attacks between two characters, turn after turn.
  Therefore, it is made of characters and a list of events.
  There is also some metadata, preventing some computations (reducing the event list).
  """
  @type attack_status :: {:critical, integer} | {:success, integer} | :failure
  @type battle_event ::
          {:start, Arena.Character.t(), Arena.Character.t()}
          | {:new_turn, integer}
          | {:challenger_roll, integer}
          | {:challenger_attack, attack_status}
          | {:opponent_roll, integer}
          | {:opponent_attack, attack_status}
          | {:end, :victory}
          | {:end, :defeat}
          | {:end, :draw}

  @type t :: %__MODULE__{
          challenger: Arena.Character.t(),
          opponent: Arena.Character.t(),
          turn_count: integer(),
          total_challenger_damage: integer(),
          total_opponent_damage: integer(),
          events: list(battle_event())
        }
  @enforce_keys [:challenger, :opponent]
  defstruct [
    :challenger,
    :opponent,
    turn_count: 0,
    total_challenger_damage: 0,
    total_opponent_damage: 0,
    events: []
  ]

  def outcome(%__MODULE__{events: [{:end, :victory} | _]}), do: :victory
  def outcome(%__MODULE__{events: [{:end, :defeat} | _]}), do: :defeat
  def outcome(%__MODULE__{events: [{:end, :draw} | _]}), do: :draw
end
