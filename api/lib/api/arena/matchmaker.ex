defmodule Api.Arena.Matchmaker do
  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Arena.BattleResult
  alias Api.Arena.Character

  @doc """
  Returns a list of potential opponents for a given challenger.

  They must not have fought in the past hour.

  Characters are returned with their rank distance from the challenger.

  ## Examples

  iex> list_battle_opponents(123)
  [%{character: %Character{}, rank_distance: 4}, %{character: %Character{}, rank_distance: 9}]

  """
  def list_battle_opponents(%Character{
        rank: character_rank,
        player_id: player_id
      }) do
    from(c in Character,
      left_join: br in BattleResult,
      on: c.id == br.challenger_id or c.id == br.opponent_id,
      where: c.player_id != ^player_id,
      group_by: [c.id],
      having: max(br.resolved_at) < ago(1, "hour") or is_nil(max(br.resolved_at)),
      order_by: [(c.rank - ^character_rank) * (c.rank - ^character_rank)],
      select: %{
        character: c,
        rank_distance: (c.rank - ^character_rank) * (c.rank - ^character_rank)
      }
    )
    |> Repo.all()
  end

  @doc """
  fn e -> e.count end
  fn e -> e.rank_distance end
  """
  def narrow_opponents_by_rank([]), do: {:error, :no_opponent}
  def narrow_opponents_by_rank([opponent]), do: {:ok, [opponent.character]}

  def narrow_opponents_by_rank(opponents) do
    lowest =
      opponents
      |> Enum.min_by(fn e -> e.rank_distance end, fn -> [] end)

    {:ok,
     opponents
     |> Enum.filter(fn e -> e.rank_distance == lowest.rank_distance end)
     |> Enum.map(fn e -> e.character end)}
  end

  def narrow_opponents_by_fight_count([]), do: {:error, :no_opponent}
  def narrow_opponents_by_fight_count([opponent]), do: {:ok, [opponent.character]}

  def narrow_opponents_by_fight_count(opponents) do
    lowest = opponents |> Enum.min_by(fn e -> e.count end, fn -> [] end)

    {:ok,
     opponents
     |> Enum.filter(fn e -> e.count == lowest.count end)
     |> Enum.map(fn e -> e.character end)}
  end

  def get_random_opponent([]), do: {:error, :no_opponent}
  def get_random_opponent([opponent]), do: {:ok, opponent}
  def get_random_opponent(opponents), do: {:ok, opponents |> Enum.random()}

  @doc """
  Returns a list of opponent characters with their fight count against a given character.

  Characters are returned with their rank distance from the character.

  ## Examples

  iex> list_fight_counts_against_opponents(%Character{}, [%Character{}, %Character{}])
  [%{character: %Character{...}, count: 4}, %{character: %Character{...}, count: 5}]

  """
  def list_fight_counts_against_opponents(%Character{id: character_id}, opponents) do
    ids = opponents |> Enum.map(fn e -> e.id end)

    results =
      from(c in Character,
        left_join: br in BattleResult,
        on: br.opponent_id == c.id,
        where: br.challenger_id == ^character_id or is_nil(br.challenger_id),
        where: c.id in ^ids,
        group_by: [c.id],
        select: %{character: c, count: count(br.id)}
      )
      |> Repo.all()

    opponents
    |> Enum.map(fn o ->
      Enum.find(results, %{character: o, count: 0}, fn r -> r.character.id === o.id end)
    end)
  end
end
