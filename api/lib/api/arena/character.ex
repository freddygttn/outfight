defmodule Api.Arena.Character do
  @moduledoc """
  Describes a character fighting in the Arena.
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Accounts.User

  @timestamps_opts [type: :utc_datetime]

  schema "characters" do
    field :attack, :integer
    field :defense, :integer
    field :health_points, :integer
    field :magik, :integer
    field :name, :string
    field :rank, :integer
    field :skill_points_earned, :integer
    belongs_to :player, User

    timestamps()
  end

  @doc """
  Character changeset for creation.

  Only its name and its player_id are meaningfull. Other attributes are always the same.
  """
  def create_changeset(character, attrs) do
    character
    |> cast(attrs, [
      :name,
      :player_id,
      :rank,
      :skill_points_earned,
      :health_points,
      :attack,
      :defense,
      :magik
    ])
    |> validate_required([
      :name,
      :player_id,
      :rank,
      :skill_points_earned,
      :health_points,
      :attack,
      :defense,
      :magik
    ])
    |> validate_length(:name, min: 2, max: 60)
    |> validate_number(:rank, equal_to: 1)
    |> validate_number(:skill_points_earned, equal_to: 12)
    |> validate_number(:health_points, equal_to: 10)
    |> validate_number(:attack, equal_to: 0)
    |> validate_number(:defense, equal_to: 0)
    |> validate_number(:magik, equal_to: 0)
  end

  @doc """
  Character changeset for attributes update.
  """
  def update_attribute_changeset(character, attrs) do
    character
    |> cast(attrs, [
      :health_points,
      :attack,
      :defense,
      :magik
    ])
    |> validate_number(:health_points, greater_than_or_equal_to: character.health_points)
    |> validate_number(:attack, greater_than_or_equal_to: character.attack)
    |> validate_number(:defense, greater_than_or_equal_to: character.defense)
    |> validate_number(:magik, greater_than_or_equal_to: character.magik)
    |> Api.Arena.CharacterValidator.validate_skill_points()
  end

  @doc """
  Character changeset for rank update.
  """
  def update_rank_changeset(character, attrs) do
    character
    |> cast(attrs, [
      :rank,
      :skill_points_earned
    ])
    |> validate_number(:rank, greater_than_or_equal_to: 1)
    |> validate_number(:skill_points_earned,
      greater_than_or_equal_to: character.skill_points_earned
    )
  end
end
