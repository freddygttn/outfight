defmodule Api.Arena.BattleResult do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Arena.Character

  @timestamps_opts [type: :utc_datetime]

  schema "battle_results" do
    field :outcome, Ecto.Enum, values: [:victory, :defeat, :draw]
    field :resolved_at, :utc_datetime
    belongs_to :challenger, Character
    belongs_to :opponent, Character

    timestamps()
  end

  @doc false
  def changeset(battle_result, attrs) do
    battle_result
    |> cast(attrs, [:outcome, :resolved_at, :challenger_id, :opponent_id])
    |> validate_required([:outcome, :resolved_at, :challenger_id, :opponent_id])
    |> validate_inclusion(:outcome, [:victory, :defeat, :draw])
  end
end
