defmodule Api.Arena do
  @moduledoc """
  The Arena context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Arena.BattleResult
  alias Api.Arena.Character

  @doc """
  Returns the list of characters that belongs to a player (aka user).

  ## Examples

      iex> list_player_characters(123)
      [%Character{}, ...]

  """
  def list_player_characters(player_id) do
    from(c in Character,
      where: c.player_id == ^player_id,
      order_by: c.inserted_at
    )
    |> Repo.all()
  end

  @doc """
  Counts characters slots available for a player.

  Returns an {:ok, count} or an {:error, :limit_reached} tuples.

  ## Examples

      iex> available_character_slots(123)
      {:ok, 8}

      iex> available_character_slots(456)
      {:error, :limit_reached}

  """
  def available_character_slots(player_id) do
    count =
      from(c in Character,
        where: c.player_id == ^player_id
      )
      |> Repo.aggregate(:count)

    if count < 10,
      do: {:ok, 10 - count},
      else: {:error, :limit_reached}
  end

  @doc """
  Gets a single character.

  Raises `Ecto.NoResultsError` if the Character does not exist.

  ## Examples

      iex> get_character!(123)
      %Character{}

      iex> get_character!(456)
      ** (Ecto.NoResultsError)

  """
  def get_character!(id), do: Repo.get!(Character, id)

  @doc """
  Gets a single character that belong to a player.

  Raises `Ecto.NoResultsError` if the Character does not exist.

  ## Examples

      iex> get_player_character!(123, 123)
      %Character{}

      iex> get_character!(456, 456)
      ** (Ecto.NoResultsError)

  """
  def get_player_character!(player_id, id) do
    from(c in Character,
      where: c.id == ^id,
      where: c.player_id == ^player_id
    )
    |> Repo.one!()
  end

  @doc """
  Creates a character.

  ## Examples

      iex> create_character(%{field: value})
      {:ok, %Character{}}

      iex> create_character(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_character(attrs \\ %{}) do
    %Character{
      rank: 1,
      skill_points_earned: 12,
      health_points: 10,
      attack: 0,
      defense: 0,
      magik: 0
    }
    |> Character.create_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a character attributes. (Health points, attack, defense or magik)

  ## Examples

      iex> update_character_attributes(character, %{field: new_value})
      {:ok, %Character{}}

      iex> update_character_attributes(character, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_character_attributes(%Character{} = character, attrs) do
    character
    |> Character.update_attribute_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Updates a character rank.

  ## Examples

      iex> update_character_rank(character, %{field: new_value})
      {:ok, %Character{}}

      iex> update_character_rank(character, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_character_rank(%Character{} = character, attrs) do
    character
    |> Character.update_rank_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a character.

  ## Examples

      iex> delete_character(character)
      {:ok, %Character{}}

      iex> delete_character(character)
      {:error, %Ecto.Changeset{}}

  """
  def delete_character(%Character{} = character) do
    Repo.delete(character)
  end

  @doc """
  Checks if a character is able to fight.

  Returns a boolean.

  ## Examples

      iex> is_character_able_to_fight(123)
      true

      iex> is_character_able_to_fight(456)
      false

  """
  def is_character_able_to_fight?(character_id) do
    recent_lost_battle_count =
      from(br in BattleResult,
        where: br.challenger_id == ^character_id,
        where: br.outcome == :defeat,
        where: br.resolved_at >= ago(1, "hour")
      )
      |> Repo.aggregate(:count)

    recent_lost_battle_count == 0
  end

  @doc """
  Returns the list of battle_results for a given character.

  ## Examples

      iex> list_character_battle_results(123)
      [%BattleResult{}, ...]

  """
  def list_character_battle_results(character_id) do
    from(br in BattleResult,
      where: br.challenger_id == ^character_id,
      order_by: [desc: br.resolved_at],
      limit: 5
    )
    |> Repo.all()
  end

  @doc """
  Creates a battle_result.

  ## Examples

      iex> create_battle_result(%{field: value})
      {:ok, %BattleResult{}}

      iex> create_battle_result(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_battle_result(attrs \\ %{}) do
    %BattleResult{}
    |> BattleResult.changeset(attrs)
    |> Repo.insert()
  end
end
