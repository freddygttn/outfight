defmodule Api.ArenaTest do
  use Api.DataCase

  import Api.Factory

  alias Api.Arena
  alias Api.Arena.Character
  alias Api.Arena.BattleResult

  describe "characters" do
    test "list_player_characters/1 returns all characters" do
      user = insert(:user)
      insert_list(10, :character, player: user)
      assert Arena.list_player_characters(user.id) |> Enum.count() == 10
    end

    test "get_character!/1 returns the character with given id" do
      character = insert(:character)
      fetched_character = Arena.get_character!(character.id)
      assert character.id == fetched_character.id
      assert character.name == fetched_character.name
    end

    test "get_player_character!/2 returns the character with given id" do
      user = insert(:user)
      character = insert(:character, player: user)
      assert %Character{} = Arena.get_player_character!(user.id, character.id)
    end

    test "get_player_character!/2 raises when its the wrong owner" do
      user = insert(:user)
      character = insert(:character)

      assert_raise Ecto.NoResultsError, fn ->
        Arena.get_player_character!(user.id, character.id)
      end
    end

    test "create_character/1 with valid data creates a character" do
      user = insert(:user)
      name = Faker.Superhero.En.name()

      assert {:ok, %Character{} = character} =
               Arena.create_character(%{name: name, player_id: user.id})

      assert character.name == name
      assert character.rank == 1
      assert character.skill_points_earned == 12
      assert character.health_points == 10
      assert character.attack == 0
      assert character.defense == 0
      assert character.magik == 0
      assert character.player_id == user.id
    end

    test "create_character/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Arena.create_character(%{name: nil})
    end

    test "update_character_rank/2 with valid data updates the character" do
      character = insert(:character)

      update_attrs = %{
        rank: character.rank + 1,
        skill_points_earned: character.skill_points_earned + 1
      }

      assert {:ok, %Character{} = new_character} =
               Arena.update_character_rank(character, update_attrs)

      assert new_character.attack == character.attack
      assert new_character.defense == character.defense
      assert new_character.health_points == character.health_points
      assert new_character.magik == character.magik
      assert new_character.name == character.name
      assert new_character.rank == update_attrs.rank
      assert new_character.skill_points_earned == update_attrs.skill_points_earned
    end

    test "update_character_rank/2 with invalid data returns error changeset" do
      character = insert(:character)

      update_attrs = %{
        rank: -1
      }

      assert {:error, %Ecto.Changeset{}} = Arena.update_character_rank(character, update_attrs)

      fetched_character = Arena.get_character!(character.id)
      assert character.rank === fetched_character.rank
    end

    test "delete_character/1 deletes the character" do
      character = insert(:character)
      assert {:ok, %Character{}} = Arena.delete_character(character)
      assert_raise Ecto.NoResultsError, fn -> Arena.get_character!(character.id) end
    end
  end

  describe "available_character_slots/1" do
    test "returns the available slots count" do
      user = insert(:user)
      insert_pair(:character, %{player: user})

      assert {:ok, 8} = Arena.available_character_slots(user.id)
    end

    test "returns a limit_reached error" do
      user = insert(:user)
      insert_list(10, :character, player: user)

      assert {:error, :limit_reached} = Arena.available_character_slots(user.id)
    end
  end

  describe "is_character_able_to_fight?/1" do
    test "returns true if character has no fight" do
      character = insert(:character)

      assert Arena.is_character_able_to_fight?(character.id)
    end

    test "returns true if character has not lost a fight recently" do
      character = insert(:character)

      after_one_hour =
        DateTime.utc_now()
        |> DateTime.add(-90 * 60, :second)

      insert(:battle_result, challenger: character, outcome: :defeat, resolved_at: after_one_hour)

      assert Arena.is_character_able_to_fight?(character.id)
    end

    test "returns false if a character has lost a fight recently" do
      character = insert(:character)

      before_one_hour =
        DateTime.utc_now()
        |> DateTime.add(-30 * 60, :second)

      insert(:battle_result, challenger: character, outcome: :defeat, resolved_at: before_one_hour)

      refute Arena.is_character_able_to_fight?(character.id)
    end
  end

  describe "battle_results" do
    test "list_character_battle_results/1 returns all battle_results" do
      battle_result = insert(:battle_result)

      assert Arena.list_character_battle_results(battle_result.challenger_id)
             |> Enum.count() == 1
    end

    test "create_battle_result/1 with valid data creates a battle_result" do
      challenger = insert(:character)
      opponent = insert(:character)

      create_attrs =
        params_for(:battle_result, challenger_id: challenger.id, opponent_id: opponent.id)

      assert {:ok, %BattleResult{} = battle_result} = Arena.create_battle_result(create_attrs)
      assert battle_result.outcome == create_attrs.outcome
    end

    test "create_battle_result/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Arena.create_battle_result(%{outcome: nil})
    end
  end
end
