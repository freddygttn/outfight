defmodule Api.Arena.CharacterValidatorTest do
  use ExUnit.Case

  alias Api.Arena.Character

  doctest Api.Arena.CharacterValidator

  def basic_character() do
    %Character{
      name: "Challenger",
      rank: 1,
      skill_points_earned: 12,
      health_points: 10,
      attack: 0,
      defense: 0,
      magik: 0,
      player_id: nil
    }
  end

  describe "update_attribute_changeset/2" do
    test "validates only attributes that are higher or equal" do
      current = basic_character()

      hp_changeset =
        current
        |> Character.update_attribute_changeset(%{health_points: 9})

      attack_changeset =
        current
        |> Character.update_attribute_changeset(%{attack: -1})

      defense_changeset =
        current
        |> Character.update_attribute_changeset(%{defense: -1})

      magik_changeset =
        current
        |> Character.update_attribute_changeset(%{magik: -1})

      refute hp_changeset.valid?
      refute attack_changeset.valid?
      refute defense_changeset.valid?
      refute magik_changeset.valid?
    end

    test "validates consistent skill points" do
      current = basic_character()
      # 2 + 4 + 4 + 3 = 13
      invalid_updates = %{health_points: 12, attack: 4, defense: 4, magik: 3}
      # 2 + 4 + 3 + 3 = 12
      valid_updates = %{health_points: 12, attack: 4, defense: 3, magik: 3}

      invalid_changeset =
        current
        |> Character.update_attribute_changeset(invalid_updates)

      valid_changeset =
        current
        |> Character.update_attribute_changeset(valid_updates)

      refute invalid_changeset.valid?
      assert valid_changeset.valid?
    end
  end
end
