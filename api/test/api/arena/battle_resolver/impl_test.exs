defmodule Api.BattleResolver.ImplTest do
  use ExUnit.Case
  alias Api.Arena.Battle
  alias Api.Arena.BattleResolver.Impl
  alias Api.Arena.Character

  doctest Api.Arena.BattleResolver.Impl

  describe "new_turn/1" do
    test "increments and creates an event" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, turn_count: 1}

      %Battle{turn_count: turn_count, events: [e | _]} = Impl.new_turn(battle)
      assert turn_count === 2
      assert e === {:new_turn, 2}
    end

    test "does nothing after battle ended" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, turn_count: 1, events: [{:end, :victory}]}

      new_battle = Impl.new_turn(battle)
      assert battle === new_battle
    end
  end

  describe "challenger_roll/1" do
    test "rolls and creates an event" do
      a = %Character{name: "Challenger", attack: 5}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b}

      %Battle{events: [{:challenger_roll, roll} | _]} = Impl.challenger_roll(battle)
      assert roll >= 1 && roll <= 5
    end

    test "rolls 0" do
      a = %Character{name: "Challenger", attack: 0}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b}

      %Battle{events: [{:challenger_roll, roll} | _]} = Impl.challenger_roll(battle)
      assert roll === 0
    end

    test "does nothing when battle ended" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, events: [{:end, :victory}]}

      new_battle = Impl.challenger_roll(battle)
      assert battle === new_battle
    end
  end

  describe "opponent_roll/1" do
    test "rolls and creates an event" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent", attack: 5}
      battle = %Battle{challenger: a, opponent: b}

      %Battle{events: [{:opponent_roll, roll} | _]} = Impl.opponent_roll(battle)
      assert roll >= 1 && roll <= 5
    end

    test "rolls 0" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent", attack: 0}
      battle = %Battle{challenger: a, opponent: b}

      %Battle{events: [{:opponent_roll, roll} | _]} = Impl.opponent_roll(battle)
      assert roll === 0
    end

    test "does nothing when battle ended" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, events: [{:end, :victory}]}

      new_battle = Impl.opponent_roll(battle)
      assert battle === new_battle
    end
  end

  describe "attack/1" do
    test "should fail" do
      a = %Character{name: "Challenger", attack: 5}
      b = %Character{name: "Opponent", defense: 10}
      battle = %Battle{challenger: a, opponent: b, events: [{:challenger_roll, 5}]}

      %Battle{events: [{:challenger_attack, result} | _]} = Impl.attack(battle)
      assert result === :failure
    end

    test "should succeed" do
      a = %Character{name: "Challenger", attack: 5, magik: 0}
      b = %Character{name: "Opponent", defense: 0}
      battle = %Battle{challenger: a, opponent: b, events: [{:challenger_roll, 5}]}

      %Battle{events: [{:challenger_attack, result} | _]} = Impl.attack(battle)
      assert result === {:success, 5}
    end

    test "should crit" do
      a = %Character{name: "Challenger", attack: 5, magik: 5}
      b = %Character{name: "Opponent", defense: 0}
      battle = %Battle{challenger: a, opponent: b, events: [{:challenger_roll, 5}]}

      %Battle{events: [{:challenger_attack, result} | _]} = Impl.attack(battle)
      assert result === {:critical, 10}
    end

    test "does nothing when battle ended" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, events: [{:end, :victory}]}

      new_battle = Impl.attack(battle)
      assert battle === new_battle
    end
  end

  describe "apply_attack/1" do
    test "updates total damage on success" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}

      battle = %Battle{
        challenger: a,
        opponent: b,
        total_challenger_damage: 0,
        events: [{:challenger_attack, {:success, 5}}]
      }

      %Battle{total_challenger_damage: dmg} = Impl.apply_attack(battle)
      assert dmg === 5
    end

    test "updates total damage on crit" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}

      battle = %Battle{
        challenger: a,
        opponent: b,
        total_challenger_damage: 0,
        events: [{:challenger_attack, {:critical, 6}}]
      }

      %Battle{total_challenger_damage: dmg} = Impl.apply_attack(battle)
      assert dmg === 6
    end

    test "does not update total damage on failure" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}

      battle = %Battle{
        challenger: a,
        opponent: b,
        total_challenger_damage: 0,
        events: [{:challenger_attack, :failure}]
      }

      %Battle{total_challenger_damage: dmg} = Impl.apply_attack(battle)
      assert dmg === 0
    end

    test "does nothing when battle ended" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, events: [{:end, :victory}]}

      new_battle = Impl.apply_attack(battle)
      assert battle === new_battle
    end
  end

  describe "check_for_end/1" do
    test "resolves to a victory" do
      a = %Character{name: "Challenger", health_points: 12}
      b = %Character{name: "Opponent", health_points: 12}
      battle = %Battle{challenger: a, opponent: b, total_challenger_damage: 12}

      %Battle{events: [e | _]} = Impl.check_for_end(battle)
      assert e === {:end, :victory}
    end

    test "resolves to a defeat" do
      a = %Character{name: "Challenger", health_points: 12}
      b = %Character{name: "Opponent", health_points: 12}
      battle = %Battle{challenger: a, opponent: b, total_opponent_damage: 12}

      %Battle{events: [e | _]} = Impl.check_for_end(battle)
      assert e === {:end, :defeat}
    end

    test "resolves to a draw because of challenger stats after 3 turns" do
      a = %Character{name: "Challenger", health_points: 12, attack: 2, defense: 2}
      b = %Character{name: "Opponent", health_points: 12, attack: 2, defense: 2}
      battle = %Battle{challenger: a, opponent: b, turn_count: 3}

      %Battle{events: [e | _]} = Impl.check_for_end(battle)
      assert e === {:end, :draw}
    end

    test "resolves to a draw because of boring match" do
      a = %Character{name: "Challenger", health_points: 12, attack: 101, defense: 100}
      b = %Character{name: "Opponent", health_points: 12, attack: 101, defense: 100}

      battle = %Battle{
        challenger: a,
        opponent: b,
        turn_count: 5,
        total_challenger_damage: 0,
        total_opponent_damage: 0
      }

      %Battle{events: [e | _]} = Impl.check_for_end(battle)
      assert e === {:end, :draw}
    end

    test "does nothing when battle ended" do
      a = %Character{name: "Challenger"}
      b = %Character{name: "Opponent"}
      battle = %Battle{challenger: a, opponent: b, events: [{:end, :victory}]}

      new_battle = Impl.check_for_end(battle)
      assert battle === new_battle
    end
  end
end
