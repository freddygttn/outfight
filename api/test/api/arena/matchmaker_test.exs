defmodule Api.Arena.MatchmakerTest do
  use Api.DataCase

  import Api.Factory

  alias Api.Arena.Character
  alias Api.Arena.Matchmaker

  describe "list_battle_opponents/1" do
    test "returns a character and rank distance from the challenger" do
      challenger = insert(:character)
      insert(:character)

      assert [%{character: %Character{}, rank_distance: _}] =
               Matchmaker.list_battle_opponents(challenger)
    end

    test "ignores characters that fought the past hour" do
      challenger = insert(:character)
      opponent = insert(:character)
      insert(:battle_result, opponent: opponent, resolved_at: DateTime.utc_now())

      assert [] = Matchmaker.list_battle_opponents(challenger)
    end
  end

  describe "narrow_opponents_by_rank/1" do
    test "returns only characters with lowest rank distance" do
      opponents_with_distance =
        20..40
        |> Enum.to_list()
        |> Enum.map(fn n ->
          %{character: build(:character), rank_distance: Integer.floor_div(n, 10)}
        end)

      {:ok, results} = Matchmaker.narrow_opponents_by_rank(opponents_with_distance)

      assert results |> Enum.count() == 10
    end

    test "returns a Character" do
      assert {:ok, [%Character{}]} =
               Matchmaker.narrow_opponents_by_rank([
                 %{character: build(:character), rank_distance: 1}
               ])
    end

    test "returns an error on empty list" do
      assert {:error, _} = Matchmaker.narrow_opponents_by_rank([])
    end
  end

  describe "narrow_opponents_by_fight_count/1" do
    test "returns only characters with lowest count" do
      opponents_with_count =
        20..40
        |> Enum.to_list()
        |> Enum.map(fn n ->
          %{character: build(:character), count: Integer.floor_div(n, 10)}
        end)

      {:ok, results} = Matchmaker.narrow_opponents_by_fight_count(opponents_with_count)

      assert results |> Enum.count() == 10
    end

    test "returns a Character" do
      assert {:ok, [%Character{}]} =
               Matchmaker.narrow_opponents_by_fight_count([
                 %{character: build(:character), count: 1}
               ])
    end

    test "returns an error on empty list" do
      assert {:error, _} = Matchmaker.narrow_opponents_by_fight_count([])
    end
  end

  describe "list_fight_counts_against_opponents/2" do
    test "returns a character and its fight count against the challenger" do
      challenger = insert(:character)
      opponent = insert(:character)
      insert_pair(:battle_result, challenger: challenger, opponent: opponent)

      assert [%{character: %Character{}, count: 2}] =
               Matchmaker.list_fight_counts_against_opponents(challenger, [opponent])
    end

    test "returns when there is no previous battles between them" do
      challenger = insert(:character)
      opponent = insert(:character)
      insert(:battle_result, opponent: opponent)

      assert [%{character: %Character{}, count: 0}] =
               Matchmaker.list_fight_counts_against_opponents(challenger, [opponent])
    end
  end
end
