defmodule Api.BattleResolver.Test do
  use ExUnit.Case

  import Api.Factory

  alias Api.Arena.Battle
  alias Api.Arena.BattleResolver

  describe "resolve_battle/2" do
    test "starts with a start event" do
      [a, b] = build_pair(:character)

      %Battle{events: events} = BattleResolver.resolve_battle(a, b)
      assert [{:start, ^a, ^b} | _] = events |> Enum.reverse()
    end

    test "ends with a end event" do
      [a, b] = build_pair(:character)

      assert %Battle{events: [{:end, _} | _]} = BattleResolver.resolve_battle(a, b)
    end
  end
end
